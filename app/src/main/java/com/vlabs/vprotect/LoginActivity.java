package com.vlabs.vprotect;

import android.Manifest;
import android.app.ProgressDialog;
import android.bluetooth.BluetoothAdapter;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentSender;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResult;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;
import com.google.gson.Gson;
import com.vlabs.vprotect.Util.Util;
import com.vlabs.vprotect.model.AppUser;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class LoginActivity extends AppCompatActivity {

    private static final String TAG = LoginActivity.class.getName();
    private Button btnLogin = null;
    private EditText editUsername = null;
    private EditText editPassword = null;
    private Context context;
    private Util util;
    private BluetoothAdapter mBluetoothAdapter;
    private LocationManager mLocationManager = null;

    private int REQUEST_ENABLE_BT = 1111;
    private ProgressDialog progDailog;
    private String response;
    private GoogleApiClient googleApiClient;
    final static int REQUEST_LOCATION = 199;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        context = this;
        util = new Util();

        mLocationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);

        try {
            mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
            if(!mBluetoothAdapter.isEnabled())
            {
                Intent enableBtIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
                startActivityForResult(enableBtIntent, REQUEST_ENABLE_BT);
            }

            googleApiClient = new GoogleApiClient.Builder(LoginActivity.this)
                    .addApi(LocationServices.API)
                    .addConnectionCallbacks(new GoogleApiClient.ConnectionCallbacks() {
                        @Override
                        public void onConnected(Bundle bundle) {

                        }

                        @Override
                        public void onConnectionSuspended(int i) {
                            googleApiClient.connect();
                        }
                    })
                    .addOnConnectionFailedListener(new GoogleApiClient.OnConnectionFailedListener() {
                        @Override
                        public void onConnectionFailed(ConnectionResult connectionResult) {

                            Log.d("Location error","Location error " + connectionResult.getErrorCode());
                        }
                    }).build();
            googleApiClient.connect();

            LocationRequest locationRequest = LocationRequest.create();
            locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
            locationRequest.setInterval(0);
            LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder()
                    .addLocationRequest(locationRequest);

            builder.setAlwaysShow(true);

            PendingResult<LocationSettingsResult> result =
                    LocationServices.SettingsApi.checkLocationSettings(googleApiClient, builder.build());
            result.setResultCallback(new ResultCallback<LocationSettingsResult>() {
                @Override
                public void onResult(LocationSettingsResult result) {
                    final Status status = result.getStatus();
                    switch (status.getStatusCode()) {
                        case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                            try {
                                // Show the dialog by calling startResolutionForResult(),
                                // and check the result in onActivityResult().
                                status.startResolutionForResult(LoginActivity.this, REQUEST_LOCATION);
                            } catch (IntentSender.SendIntentException e) {
                                // Ignore the error.
                            }
                            break;
                    }
                }
            });

        }
        catch (Exception e)
        {

        }

        try {
            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED || ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED || ActivityCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED || ActivityCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED || ActivityCompat.checkSelfPermission(this, Manifest.permission.BLUETOOTH) != PackageManager.PERMISSION_GRANTED || ActivityCompat.checkSelfPermission(this, Manifest.permission.BLUETOOTH_ADMIN) != PackageManager.PERMISSION_GRANTED) {
                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.BLUETOOTH, Manifest.permission.BLUETOOTH_ADMIN, Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE},
                        199);

            }
        } catch (Exception e) {

        }

        editUsername = (EditText) findViewById(R.id.editUsername);
        editPassword = (EditText) findViewById(R.id.editPassword);

        btnLogin = (Button) findViewById(R.id.btnLogin);

        FirebaseInstanceId.getInstance().getInstanceId()
                .addOnCompleteListener(new OnCompleteListener<InstanceIdResult>() {
                    @Override
                    public void onComplete(@NonNull Task<InstanceIdResult> task) {
                        if (!task.isSuccessful()) {
                            Log.w(TAG, "getInstanceId failed", task.getException());
                            return;
                        }

                        // Get new Instance ID token
                        String token = task.getResult().getToken();

                        SharedPreferences settings = getSharedPreferences("REG_TOKEN", 0);
                        SharedPreferences.Editor editor = settings.edit();
                        editor.putString("token",token );
                        editor.commit();
                    }
                });

        // Code to check if user is already logged in
        SharedPreferences pref = getApplicationContext().getSharedPreferences("LoginData", 0); // 0 - for private mode
        String strLoggedInUser = pref.getString("loggedinuser", "");

        if (!strLoggedInUser.isEmpty()) {

            Gson gson = new Gson();
            String loggedinuser  = pref.getString("loggedinuser","");
            boolean isLetterSubmitted  = pref.getBoolean("isLetterSubmitted",false);
            AppUser appUser = gson.fromJson(loggedinuser, AppUser.class);

            String usrConsentYn = appUser.usrConsentYn;

            if ((usrConsentYn != null && !usrConsentYn.equals("") && !usrConsentYn.equals("null")) || isLetterSubmitted) {
                Intent intent = new Intent(LoginActivity.this, BeaconActivity.class);
                startActivity(intent);
                finish();
            } else {
                Intent intent = new Intent(LoginActivity.this, ConsentLetterActivity.class);
                startActivity(intent);
                finish();
            }
        }

        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                // Check if Username and password are valid or not

                // Instantiate the RequestQueue.
                RequestQueue queue = Volley.newRequestQueue(context);
                String url ="http://3.6.107.191:8080/authenticateUser?usrName="+ editUsername.getText().toString() + "&usrPassword=" + editPassword.getText().toString();

                StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                        new Response.Listener<String>() {
                            @Override
                            public void onResponse(String response) {

                                if(!response.isEmpty())
                                {
                                    SharedPreferences pref = getApplicationContext().getSharedPreferences("LoginData", 0); // 0 - for private mode
                                    SharedPreferences.Editor editor = pref.edit();

                                    editor.putString("loggedinuser", response);
                                    editor.commit();

                                    Gson gson = new Gson();
                                    String loggedinuser  = pref.getString("loggedinuser","");
                                    AppUser appUser = gson.fromJson(loggedinuser, AppUser.class);

                                    int usrId = appUser.usrId;

                                    SharedPreferences settings = getSharedPreferences("REG_TOKEN", 0);
                                    String token = settings.getString("token", "null");

                                    saveFCMTokenAndProceed(usrId, token);
                                }
                                else
                                {
                                    Toast.makeText(context,"Login Unsuccessful ! Please check credentials!",Toast.LENGTH_LONG).show();
                                }

                            }
                        }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                        Toast.makeText(context,error.getMessage(),Toast.LENGTH_LONG).show();
                    }
                });
                // Add the request to the RequestQueue.
                queue.add(stringRequest);

//                if ("sagar123".equals(editPassword.getText().toString())) {
//                    //Toast.makeText(LoginActivity.this, "Welcome " + value.displayName, Toast.LENGTH_LONG).show();
//
//                    // Put it in SharedPreference
//                    SharedPreferences pref = getApplicationContext().getSharedPreferences("LoginData", 0); // 0 - for private mode
//                    SharedPreferences.Editor editor = pref.edit();
////                    Gson gson = new Gson();
////                    editor.putString("loggedinuser", gson.toJson(value));
////                    editor.commit();
//
//                    Intent intent = new Intent(LoginActivity.this, BeaconActivity.class);
//                    startActivity(intent);
//                    finish();
//
//                } else {
//                    Toast.makeText(LoginActivity.this, "Login Unsuccessful! Please check the credentials!", Toast.LENGTH_LONG).show();
//
//                }
            }
        });
    }

    public void saveFCMTokenAndProceed(final int usrId, final String token) {

        Thread t1 = new Thread(new Runnable() {
            @Override
            public void run() {

                runOnUiThread(new Runnable() {
                    public void run() {
                        progDailog = new ProgressDialog(context);
                        progDailog.setMessage("Sending data to server...");
                        progDailog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
                        progDailog.setCancelable(false);
                        progDailog.setIndeterminate(false);
                        progDailog.show();
                    }
                });

                // Instantiate the RequestQueue.
                RequestQueue queue = Volley.newRequestQueue(context);
                String url ="http://3.6.107.191:8080/setUsrAndroidFcmId";

                // Request a string response from the provided URL.
                StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                        new Response.Listener<String>() {
                            @Override
                            public void onResponse(final String resp) {

                                runOnUiThread(new Runnable() {
                                    public void run() {

                                        if (progDailog != null) {
                                            progDailog.dismiss();
                                        }

                                        if (resp.equals("Success")) {

                                            new GetS3CredentialsTask().execute();

                                        } else {

                                            new AlertDialog.Builder(context)
                                                    .setTitle("vProtect")
                                                    .setCancelable(false)
                                                    .setMessage("Something went wrong! Please try again later.")
                                                    .setPositiveButton("Okay", new DialogInterface.OnClickListener() {
                                                        @Override
                                                        public void onClick(DialogInterface dialog, int which) {
                                                            dialog.dismiss();
                                                        }
                                                    }).show();
                                        }
                                    }
                                });
                            }
                        }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                        if (progDailog != null) {
                            progDailog.dismiss();
                        }

                        new AlertDialog.Builder(context)
                                .setTitle("vProtect")
                                .setCancelable(false)
                                .setMessage("Something went wrong! Please try again later.")
                                .setPositiveButton("Okay", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        dialog.dismiss();
                                    }
                                }).show();
                    }
                }){
                    @Override
                    public Map<String, String> getParams()
                    {
                        try {

                            Map<String, String> params = new HashMap<String, String>();

                            params.put("usrId", String.valueOf(usrId));
                            params.put("usrAndroidFcmId", token);

                            return params;
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        return null;
                    }
                };
                // Add the request to the RequestQueue.
                queue.add(stringRequest);

            }
        });
        t1.start();

    }

    public class GetS3CredentialsTask extends AsyncTask<String, String, String> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progDailog = new ProgressDialog(context);
            progDailog.setMessage("Loading...");
            progDailog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            progDailog.setCancelable(false);
            progDailog.setIndeterminate(false);
            progDailog.show();
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);

            try {
                if (progDailog != null) {
                    progDailog.dismiss();
                }
                if (response != null) {
                    JSONObject object1 = new JSONObject(response);
                    String status = object1.getString("status");

                    if (status.equals("SUCCESS")) {

                        String accessKey = object1.getString("accessKey");
                        String secretKey = object1.getString("secretKey");

                        final SharedPreferences sharedPreferences = getSharedPreferences("user", 0);
                        sharedPreferences.edit().putBoolean("s3Credentials", true).commit();
                        sharedPreferences.edit().putString("secretKey", secretKey).commit();
                        sharedPreferences.edit().putString("accessKey", accessKey).commit();

                        SharedPreferences pref = getApplicationContext().getSharedPreferences("LoginData", 0);
                        Gson gson = new Gson();
                        String loggedinuser  = pref.getString("loggedinuser","");
                        AppUser appUser = gson.fromJson(loggedinuser, AppUser.class);

                        String usrConsentYn = appUser.usrConsentYn;

                        if (usrConsentYn != null && !usrConsentYn.equals("") && !usrConsentYn.equals("null")) {
                            Intent intent = new Intent(LoginActivity.this, BeaconActivity.class);
                            startActivity(intent);
                            finish();
                        } else {
                            Intent intent = new Intent(LoginActivity.this, ConsentLetterActivity.class);
                            startActivity(intent);
                            finish();
                        }
                    }

                } else {
                    Toast.makeText(context, "No data found.", Toast.LENGTH_SHORT).show();
                }

            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        @Override
        protected String doInBackground(String... strings) {
            try {

                response = util.getAwsS3Credentials();

            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }
    }
}
