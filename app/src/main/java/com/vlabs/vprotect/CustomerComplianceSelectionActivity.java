package com.vlabs.vprotect;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.gson.Gson;
import com.vlabs.vprotect.Adapters.CustomerComplianceRecyclerAdapter;
import com.vlabs.vprotect.Util.Util;
import com.vlabs.vprotect.model.AppUser;
import com.vlabs.vprotect.model.ComplianceData;

import org.json.JSONArray;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

public class CustomerComplianceSelectionActivity extends AppCompatActivity {

    private Context context;
    private Util util;

    private int orgId, depId, usrId;

    private String response, selectedDate;
    private static RecyclerView.Adapter adapter;
    private RecyclerView.LayoutManager layoutManager;
    private static RecyclerView recyclerView;
    private static ArrayList<ComplianceData> complianceDataArrayList;
    private ProgressDialog progDailog;
    private LinearLayout layParent, layNoRecord, layNoInternet;
    private FloatingActionButton addCustomerCompliance;
    private SimpleDateFormat formatter;
    private AppUser appUser;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_customer_compliance_selection);

        context= this;
        util = new Util();
        complianceDataArrayList = new ArrayList<>();
        formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

        Gson gson = new Gson();
        SharedPreferences pref = this.getSharedPreferences("LoginData", 0); // 0 - for private mode
        String loggedinuser  = pref.getString("loggedinuser","");
        appUser = gson.fromJson(loggedinuser, AppUser.class);

        orgId = appUser.orgId;
        depId = appUser.depId;
        usrId = appUser.usrId;
        layParent = findViewById(R.id.layParent);
        layNoRecord = findViewById(R.id.layNoRecord);
        layNoInternet = findViewById(R.id.layNoInternet);

        addCustomerCompliance = findViewById(R.id.addCustomerCompliance);

        recyclerView = findViewById(R.id.issued_books_recycler_view);
        recyclerView.setHasFixedSize(true);

        layoutManager = new LinearLayoutManager(context);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());

        addCustomerCompliance.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(CustomerComplianceSelectionActivity.this, AddCustomerComplianceActivity.class);
                startActivity(intent);
            }
        });

        selectedDate = formatter.format(new Date());

        new GetCustomerCompliances().execute();
    }

    public void goBack(View view) {

        finish();
    }

    public class GetCustomerCompliances extends AsyncTask<String, String, String> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progDailog = new ProgressDialog(context);
            progDailog.setMessage("Sending data to server...");
            progDailog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            progDailog.setCancelable(false);
            progDailog.setIndeterminate(false);
            progDailog.show();
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);

            try {
                if (progDailog != null) {
                    progDailog.dismiss();
                }
                if (response != null) {
                    JSONArray array = new JSONArray(response);

                    if(array.length() > 0) {
                        for (int i=0; i<array.length(); i++) {

                            JSONObject jsonObject = array.getJSONObject(i);

                            ComplianceData complianceData = new ComplianceData();
                            complianceData.setName(jsonObject.getString("cusName"));
                            complianceData.setPhone(jsonObject.getString("cusPhone"));
                            complianceData.setEmail(jsonObject.getString("cusEmail"));
                            complianceData.setIn(jsonObject.getString("cusInTime"));
                            complianceData.setOut(jsonObject.getString("cusOutTime"));
                            complianceData.setPhotoUrl(jsonObject.getString("cusPhotoUrl"));
                            complianceData.setTemp(jsonObject.getString("cusTemperature"));
//                            complianceData.setSelfStatus(jsonObject.getString("cusSelfStatus"));
//                            complianceData.setFamilyStatus(jsonObject.getString("cusFamilytatus"));
                            complianceData.setSaveDate(jsonObject.getString("cusSaveDate"));

                            complianceDataArrayList.add(complianceData);

                        }

                        if(complianceDataArrayList.size() > 0) {

                            layParent.setVisibility(View.VISIBLE);
                            adapter = new CustomerComplianceRecyclerAdapter(context, complianceDataArrayList);
                            recyclerView.setAdapter(adapter);
                        }
                    } else {

                        layParent.setVisibility(View.GONE);
                        layNoRecord.setVisibility(View.VISIBLE);
                    }

                } else {
                    Toast.makeText(context, "Couldn't reach servers at the moment. Please try again later.", Toast.LENGTH_SHORT).show();
                    finish();
                }

            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        @Override
        protected String doInBackground(String... strings) {
            try {

                response = util.getCustomerCompliances(String.valueOf(orgId), String.valueOf(depId), String.valueOf(usrId), String.valueOf(selectedDate));

            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }
    }

    @Override
    protected void onRestart() {
        super.onRestart();

        finish();
        startActivity(getIntent());
    }
}
