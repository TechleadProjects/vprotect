package com.vlabs.vprotect;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;
import com.vlabs.vprotect.Util.Util;
import com.vlabs.vprotect.model.AppUser;

import java.util.HashMap;
import java.util.Map;

public class ConsentLetterActivity extends AppCompatActivity {

    private Context context;
    private ProgressDialog progDailog;
    private int orgId, depId, usrId;
    private AppUser appUser;
    private String response;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_consent_letter);

        context = this;

        Gson gson = new Gson();
        SharedPreferences pref = this.getSharedPreferences("LoginData", 0); // 0 - for private mode
        String loggedinuser  = pref.getString("loggedinuser","");
        appUser = gson.fromJson(loggedinuser, AppUser.class);

        orgId = appUser.orgId;
        depId = appUser.depId;
        usrId = appUser.usrId;

    }

    public void submitConsent(View view) {

        Thread t1 = new Thread(new Runnable() {
            @Override
            public void run() {

                runOnUiThread(new Runnable() {
                    public void run() {
                        progDailog = new ProgressDialog(context);
                        progDailog.setMessage("Sending data to server...");
                        progDailog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
                        progDailog.setCancelable(false);
                        progDailog.setIndeterminate(false);
                        progDailog.show();
                    }
                });

                // Instantiate the RequestQueue.
                RequestQueue queue = Volley.newRequestQueue(context);
                String url ="http://3.6.107.191:8080/saveConsentStatusForUser";

                // Request a string response from the provided URL.
                StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                        new Response.Listener<String>() {
                            @Override
                            public void onResponse(final String resp) {

                                runOnUiThread(new Runnable() {
                                    public void run() {

                                        if (progDailog != null) {
                                            progDailog.dismiss();
                                        }

                                        if (resp.equals("Success")) {

                                            SharedPreferences settings = getSharedPreferences("LoginData", 0);
                                            SharedPreferences.Editor editor = settings.edit();
                                            editor.putBoolean("isLetterSubmitted", true);
                                            editor.commit();

                                            Intent intent = new Intent(ConsentLetterActivity.this, BeaconActivity.class);
                                            startActivity(intent);
                                            finish();

                                        } else {

                                            new AlertDialog.Builder(context)
                                                    .setTitle("vProtect")
                                                    .setCancelable(false)
                                                    .setMessage("Something went wrong! Please try again later.")
                                                    .setPositiveButton("Okay", new DialogInterface.OnClickListener() {
                                                        @Override
                                                        public void onClick(DialogInterface dialog, int which) {
                                                            dialog.dismiss();
                                                        }
                                                    }).show();
                                        }
                                    }
                                });
                            }
                        }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        response = null;
                    }
                }){
                    @Override
                    public Map<String, String> getParams()
                    {
                        try {

                            Map<String, String>  params = new HashMap<String, String>();
                            params.put("usrId", String.valueOf(usrId));
                            params.put("consentStatusYn", "Y");

                            return params;
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        return null;
                    }
                };
                // Add the request to the RequestQueue.
                queue.add(stringRequest);

            }
        });
        t1.start();

    }
}
