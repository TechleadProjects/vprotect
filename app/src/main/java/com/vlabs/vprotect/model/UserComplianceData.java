package com.vlabs.vprotect.model;

public class UserComplianceData {

    private int uscId;
    private int chkId;
    private String chkName;
    private String uscName;
    private String chkQuestionJson;
    private String uscAnswerJson;
    private String uscAddedDate;
    private String uscScheduledEndDate;
    private String uscActualEndDate;
    private String uscStatus;
    private String uscComment;
    private String uscPhotoUrl;

    public int getChkId() {
        return chkId;
    }

    public void setChkId(int chkId) {
        this.chkId = chkId;
    }

    public String getChkName() {
        return chkName;
    }

    public void setChkName(String chkName) {
        this.chkName = chkName;
    }

    public String getUscName() {
        return uscName;
    }

    public void setUscName(String uscName) {
        this.uscName = uscName;
    }

    public String getChkQuestionJson() {
        return chkQuestionJson;
    }

    public void setChkQuestionJson(String chkQuestionJson) {
        this.chkQuestionJson = chkQuestionJson;
    }

    public String getUscAnswerJson() {
        return uscAnswerJson;
    }

    public void setUscAnswerJson(String uscAnswerJson) {
        this.uscAnswerJson = uscAnswerJson;
    }

    public String getUscAddedDate() {
        return uscAddedDate;
    }

    public void setUscAddedDate(String uscAddedDate) {
        this.uscAddedDate = uscAddedDate;
    }

    public String getUscScheduledEndDate() {
        return uscScheduledEndDate;
    }

    public void setUscScheduledEndDate(String uscScheduledEndDate) {
        this.uscScheduledEndDate = uscScheduledEndDate;
    }

    public String getUscActualEndDate() {
        return uscActualEndDate;
    }

    public void setUscActualEndDate(String uscActualEndDate) {
        this.uscActualEndDate = uscActualEndDate;
    }

    public String getUscStatus() {
        return uscStatus;
    }

    public void setUscStatus(String uscStatus) {
        this.uscStatus = uscStatus;
    }

    public String getUscComment() {
        return uscComment;
    }

    public void setUscComment(String uscComment) {
        this.uscComment = uscComment;
    }

    public String getUscPhotoUrl() {
        return uscPhotoUrl;
    }

    public void setUscPhotoUrl(String uscPhotoUrl) {
        this.uscPhotoUrl = uscPhotoUrl;
    }

    public int getUscId() {
        return uscId;
    }

    public void setUscId(int uscId) {
        this.uscId = uscId;
    }
}
