package com.vlabs.vprotect.model;

public class ComplianceData {

    private int orgId;
    private int depId;
    private int usrId;
    private String photoUrl;
    private String name;
    private String email;
    private String phone;
    private String in;
    private String out;
    private String washedHands;
    private String wearingMask;
    private String temp;
    private String selfStatus;
    private String familyStatus;
    private String saveDate;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getIn() {
        return in;
    }

    public void setIn(String in) {
        this.in = in;
    }

    public String getOut() {
        return out;
    }

    public void setOut(String out) {
        this.out = out;
    }

    public String getWashedHands() {
        return washedHands;
    }

    public void setWashedHands(String washedHands) {
        this.washedHands = washedHands;
    }

    public String getWearingMask() {
        return wearingMask;
    }

    public void setWearingMask(String wearingMask) {
        this.wearingMask = wearingMask;
    }

    public String getTemp() {
        return temp;
    }

    public void setTemp(String temp) {
        this.temp = temp;
    }

    public String getSelfStatus() {
        return selfStatus;
    }

    public void setSelfStatus(String selfStatus) {
        this.selfStatus = selfStatus;
    }

    public String getFamilyStatus() {
        return familyStatus;
    }

    public void setFamilyStatus(String familyStatus) {
        this.familyStatus = familyStatus;
    }

    public String getPhotoUrl() {
        return photoUrl;
    }

    public void setPhotoUrl(String photoUrl) {
        this.photoUrl = photoUrl;
    }

    public int getOrgId() {
        return orgId;
    }

    public void setOrgId(int orgId) {
        this.orgId = orgId;
    }

    public int getDepId() {
        return depId;
    }

    public void setDepId(int depId) {
        this.depId = depId;
    }

    public int getUsrId() {
        return usrId;
    }

    public void setUsrId(int usrId) {
        this.usrId = usrId;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getSaveDate() {
        return saveDate;
    }

    public void setSaveDate(String saveDate) {
        this.saveDate = saveDate;
    }
}
