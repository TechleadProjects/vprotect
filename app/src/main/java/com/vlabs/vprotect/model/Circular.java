package com.vlabs.vprotect.model;

public class Circular {

    private int cirId;
    private int monId;
    private int dayId;
    private String cirSubject;
    private String cirMessage;
    private String cirLink;
    private String cirFlagRu;
    private String cirDate;
    private String cirReadDate;

    public int getCirId() {
        return cirId;
    }

    public void setCirId(int cirId) {
        this.cirId = cirId;
    }

    public String getCirLink() {
        return cirLink;
    }

    public void setCirLink(String cirLink) {
        this.cirLink = cirLink;
    }

    public String getCirFlagRu() {
        return cirFlagRu;
    }

    public void setCirFlagRu(String cirFlagRu) {
        this.cirFlagRu = cirFlagRu;
    }

    public String getCirDate() {
        return cirDate;
    }

    public void setCirDate(String cirDate) {
        this.cirDate = cirDate;
    }

    public String getCirReadDate() {
        return cirReadDate;
    }

    public void setCirReadDate(String cirReadDate) {
        this.cirReadDate = cirReadDate;
    }

    public String getCirSubject() {
        return cirSubject;
    }

    public void setCirSubject(String cirSubject) {
        this.cirSubject = cirSubject;
    }

    public String getCirMessage() {
        return cirMessage;
    }

    public void setCirMessage(String cirMessage) {
        this.cirMessage = cirMessage;
    }

    public int getMonId() {
        return monId;
    }

    public void setMonId(int monId) {
        this.monId = monId;
    }

    public int getDayId() {
        return dayId;
    }

    public void setDayId(int dayId) {
        this.dayId = dayId;
    }
}
