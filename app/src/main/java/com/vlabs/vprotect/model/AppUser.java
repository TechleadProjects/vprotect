package com.vlabs.vprotect.model;

import java.io.Serializable;


//{"usrId":1,"orgId":1,"depId":1,"ugpId":2,"usrName":"deshsag","usrPassword":"deshsag","usrFullName":"Sagar Deshmukh","usrFcmAndroidId":null,"usrFcmIosId":null,"usrBluId":null,"usrBluUpdateDate":null,"trnInf":"0","usrIdSet":true}
public class AppUser implements Serializable {

    public Integer usrId;
    public Integer orgId;
    public Integer depId;
    public Integer ugpId;
    public String usrName;
    public String usrPassword;
    public String usrFullName;
    public String usrConsentYn;
    public String usrContainmentZoneStatusRog;
    public String usrInTime;
    public String usrOutTime;

    public AppUser() {
    }

    public AppUser(Integer usrId, Integer orgId, Integer depId, Integer ugpId, String usrName, String usrPassword, String usrFullName, String usrConsentYn, String usrContainmentZoneStatusRog, String usrInTime, String usrOutTime) {
        this.usrId = usrId;
        this.orgId = orgId;
        this.depId = depId;
        this.ugpId = ugpId;
        this.usrName = usrName;
        this.usrPassword = usrPassword;
        this.usrFullName = usrFullName;
        this.usrConsentYn = usrConsentYn;
        this.usrContainmentZoneStatusRog = usrContainmentZoneStatusRog;
        this.usrInTime = usrInTime;
        this.usrOutTime = usrOutTime;
    }

    public Integer getUsrId() {
        return usrId;
    }

    public void setUsrId(Integer usrId) {
        this.usrId = usrId;
    }

    public Integer getOrgId() {
        return orgId;
    }

    public void setOrgId(Integer orgId) {
        this.orgId = orgId;
    }

    public Integer getDepId() {
        return depId;
    }

    public void setDepId(Integer depId) {
        this.depId = depId;
    }

    public Integer getUgpId() {
        return ugpId;
    }

    public void setUgpId(Integer ugpId) {
        this.ugpId = ugpId;
    }

    public String getUsrName() {
        return usrName;
    }

    public void setUsrName(String usrName) {
        this.usrName = usrName;
    }

    public String getUsrPassword() {
        return usrPassword;
    }

    public void setUsrPassword(String usrPassword) {
        this.usrPassword = usrPassword;
    }

    public String getUsrFullName() {
        return usrFullName;
    }

    public void setUsrFullName(String usrFullName) {
        this.usrFullName = usrFullName;
    }

    public String getUsrConsentYn() {
        return usrConsentYn;
    }

    public void setUsrConsentYn(String usrConsentYn) {
        this.usrConsentYn = usrConsentYn;
    }

    public String getUsrContainmentZoneStatusRog() {
        return usrContainmentZoneStatusRog;
    }

    public void setUsrContainmentZoneStatusRog(String usrContainmentZoneStatusRog) {
        this.usrContainmentZoneStatusRog = usrContainmentZoneStatusRog;
    }

    public String getUsrInTime() {
        return usrInTime;
    }

    public void setUsrInTime(String usrInTime) {
        this.usrInTime = usrInTime;
    }

    public String getUsrOutTime() {
        return usrOutTime;
    }

    public void setUsrOutTime(String usrOutTime) {
        this.usrOutTime = usrOutTime;
    }
}
