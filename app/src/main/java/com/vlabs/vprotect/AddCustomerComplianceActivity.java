package com.vlabs.vprotect;

import android.app.Activity;
import android.app.ProgressDialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.RadioGroup;
import android.widget.TimePicker;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import com.amazonaws.ClientConfiguration;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.regions.Region;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3Client;
import com.amazonaws.services.s3.model.CannedAccessControlList;
import com.amazonaws.services.s3.model.PutObjectRequest;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.github.dhaval2404.imagepicker.ImagePicker;
import com.google.gson.Gson;
import com.vlabs.vprotect.Util.Util;
import com.vlabs.vprotect.model.AppUser;

import org.apache.commons.io.FilenameUtils;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import de.hdodenhof.circleimageview.CircleImageView;

public class AddCustomerComplianceActivity extends AppCompatActivity {

    private Context context;
    private Util util;

    private String response;
    private ProgressDialog progDailog;

    private CircleImageView profilePhoto;
    private Button saveDetails;

    private int orgId, depId, usrId;
    private EditText name, phone, email, in, out, temp;
    private CheckBox washedHands, wearingMask;
    private RadioGroup selfStatus, familyStatus;
    private SimpleDateFormat formatter, formatter2;

    private String enteredName = "", enteredPhone = "", enteredEmail = "", enteredIn = "", enteredOut = "", enteredTemp = "", selectedWashedHands = "", selectedWearingMask = "", selectedSelfStatus = "", selectedFamilyStatus = "", saveDate = "", profileByteString = "", s3Url = "";
    private String displayName;
    private String accessKey, secretKey;
    private AppUser appUser;
    private String checklistData;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_customer_compliance);

        context = this;
        util = new Util();
        formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        formatter2 = new SimpleDateFormat("yyyy-MM-dd");

        final SharedPreferences sharedPreferences = getSharedPreferences("user", 0);

        accessKey = sharedPreferences.getString("accessKey", null);
        secretKey = sharedPreferences.getString("secretKey", null);

        Gson gson = new Gson();
        SharedPreferences pref = this.getSharedPreferences("LoginData", 0); // 0 - for private mode
        String loggedinuser  = pref.getString("loggedinuser","");
        appUser = gson.fromJson(loggedinuser, AppUser.class);

        orgId = appUser.orgId;
        depId = appUser.depId;
        usrId = appUser.usrId;

        profilePhoto = findViewById(R.id.profilePhoto);
        saveDetails = findViewById(R.id.saveDetails);

        name = findViewById(R.id.name);
        phone = findViewById(R.id.phone);
        in = findViewById(R.id.in);
        out = findViewById(R.id.out);
        temp = findViewById(R.id.temp);
        email = findViewById(R.id.email);

        washedHands = findViewById(R.id.washedHands);
        wearingMask = findViewById(R.id.wearingMask);

        selfStatus = findViewById(R.id.selfStatus);
        familyStatus = findViewById(R.id.familyStatus);

        profilePhoto.bringToFront();

        washedHands.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if(b) {
                    selectedWashedHands = "Y";
                }else {
                    selectedWashedHands = "N";
                }
            }
        });

        wearingMask.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if(b) {
                    selectedWearingMask = "Y";
                }else {
                    selectedWearingMask = "N";
                }
            }
        });

        selfStatus.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int i) {

                if(i == R.id.safeAndHealthySelf) {

                    selectedSelfStatus = "Safe and healthy";
                } else if(i == R.id.quarantinedSelf){

                    selectedSelfStatus = "Was quarantined";
                } else if(i == R.id.hadAndRecoveredSelf){

                    selectedSelfStatus = "Had virus and have recovered from it.";
                }
            }
        });

        familyStatus.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int i) {

                if(i == R.id.safeAndHealthyFamily) {

                    selectedFamilyStatus = "Safe and healthy";
                } else if(i == R.id.quarantinedFamily){

                    selectedFamilyStatus = "Was quarantined";
                } else if(i == R.id.hadAndRecoveredFamily){

                    selectedFamilyStatus = "Had virus and have recovered from it.";
                }
            }
        });

        profilePhoto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                ImagePicker.Companion.with(AddCustomerComplianceActivity.this).crop().compress(1024).maxResultSize(1080, 1080).start();
            }
        });

        saveDetails.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                enteredName = name.getText().toString();
                enteredPhone = phone.getText().toString();
                enteredEmail = email.getText().toString();
                enteredIn = in.getText().toString();
                enteredOut = out.getText().toString();
                saveDate = formatter.format(new Date());
                enteredTemp = temp.getText().toString();

                if(profileByteString.equals("")) {

                    new androidx.appcompat.app.AlertDialog.Builder(context)
                            .setTitle("vProtect")
                            .setCancelable(false)
                            .setMessage("Customer image cannot be empty!")
                            .setPositiveButton("Okay", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.dismiss();
                                }
                            }).show();

                } else if(enteredName.equals("") || enteredPhone.equals("") || enteredEmail.equals("") || enteredIn.equals("") || enteredOut.equals("") || enteredTemp.equals("")) {

                    new androidx.appcompat.app.AlertDialog.Builder(context)
                            .setTitle("vProtect")
                            .setCancelable(false)
                            .setMessage("Please don't leave any field blank!")
                            .setPositiveButton("Okay", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.dismiss();
                                }
                            }).show();
                } else if(!isMobileNumberValid(enteredPhone)) {

                    new androidx.appcompat.app.AlertDialog.Builder(context)
                            .setTitle("vProtect")
                            .setCancelable(false)
                            .setMessage("Please enter valid phone number!")
                            .setPositiveButton("Okay", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.dismiss();
                                }
                            }).show();

                } else if(!isEmailValid(enteredEmail)) {

                    new androidx.appcompat.app.AlertDialog.Builder(context)
                            .setTitle("vProtect")
                            .setCancelable(false)
                            .setMessage("Please enter valid email address!")
                            .setPositiveButton("Okay", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.dismiss();
                                }
                            }).show();

                } else {

                    if (selectedWashedHands.equals("Y")) {
                        checklistData = "Washed Hands";
                    } else {
                        checklistData = "Not washed Hands";
                    }

                    if (selectedWearingMask.equals("Y")) {
                        checklistData = checklistData + "," + "Wearing Masks";
                    } else {
                        checklistData = checklistData + "," + "Not wearing masks";
                    }

                    Thread t1 = new Thread(new Runnable() {
                        @Override
                        public void run() {

                            runOnUiThread(new Runnable() {
                                public void run() {
                                    progDailog = new ProgressDialog(context);
                                    progDailog.setMessage("Uploading files to server...");
                                    progDailog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
                                    progDailog.setCancelable(false);
                                    progDailog.setIndeterminate(false);
                                    progDailog.show();
                                }
                            });

                            SimpleDateFormat ft = new SimpleDateFormat ("yyyy.MM.dd-hh:mm:ss:SSS");
                            String ext = FilenameUtils.getExtension(displayName);
                            final String fileName = String.valueOf(orgId).concat(String.valueOf(depId)).concat(String.valueOf(usrId).concat("_").concat(ft.format(new Date())).concat(".").concat(ext));
                            boolean status = uploadFilesToS3(profileByteString, fileName);

                            if(status) {

                                runOnUiThread(new Runnable() {
                                    public void run() {

                                        if (progDailog != null) {
                                            progDailog.dismiss();
                                        }

                                        s3Url = "http://s3.ap-southeast-1.amazonaws.com/".concat(util.getBucketName()).concat("/").concat(fileName);

                                        sendDataToServer();

                                    }
                                });
                            } else {

                                runOnUiThread(new Runnable() {
                                    public void run() {

                                        if (progDailog != null) {
                                            progDailog.dismiss();
                                        }

                                        new AlertDialog.Builder(context)
                                                .setTitle("vProtect")
                                                .setCancelable(false)
                                                .setMessage("Failed to upload the profile picture for user.")
                                                .setPositiveButton("Okay", new DialogInterface.OnClickListener() {
                                                    @Override
                                                    public void onClick(DialogInterface dialog, int which) {
                                                        dialog.dismiss();
                                                    }
                                                }).show();
                                    }
                                });
                            }

                        }
                    });
                    t1.start();
                }

            }
        });
    }

    public boolean uploadFilesToS3(String filePath, String fileName){

        try {

            ClientConfiguration clientConfiguration = new ClientConfiguration()
                    .withMaxErrorRetry(1) // 1 retries
                    .withConnectionTimeout(200000) // 30,000 ms
                    .withSocketTimeout(200000); // 30,000 ms

            AmazonS3 s3Client = new AmazonS3Client(new BasicAWSCredentials(accessKey, secretKey), clientConfiguration);
            s3Client.setRegion(Region.getRegion(Regions.AP_SOUTHEAST_1));

            PutObjectRequest por = new PutObjectRequest(util.getBucketName(), fileName, new File(filePath));
            por.withCannedAcl(CannedAccessControlList.PublicRead);
            s3Client.putObject(por);
            return true;
        } catch (Exception e){
            return false;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == Activity.RESULT_OK) {

            profilePhoto.setImageURI(data.getData());

            //You can get File object from intent
            File file = ImagePicker.Companion.getFile(data);

            if(file != null) {
                profileByteString = file.getPath();

                String[] pathArray = profileByteString.split("/");
                displayName = pathArray[pathArray.length - 1];

            } else {
                Toast.makeText(this, "Could not select Image.", Toast.LENGTH_SHORT).show();
            }

        } else if (resultCode == ImagePicker.RESULT_ERROR) {
            Toast.makeText(this, "Could not select Image.", Toast.LENGTH_SHORT).show();
        }
    }

    public void goBack(View view) {

        finish();
    }

    public void getInTime(View view) {

        // Get Current Time
        final Calendar c = Calendar.getInstance();
        int mHour = c.get(Calendar.HOUR_OF_DAY);
        int mMinute = c.get(Calendar.MINUTE);

        // Launch Time Picker Dialog
        TimePickerDialog timePickerDialog = new TimePickerDialog(this,
                new TimePickerDialog.OnTimeSetListener() {

                    @Override
                    public void onTimeSet(TimePicker view, int hourOfDay,
                                          int minute) {

                        String date = formatter2.format(new Date());
                        String time = String.valueOf(hourOfDay).concat(":").concat(String.valueOf(minute)).concat(":").concat("00");

                        try {
                            Date dateToBeSend = formatter.parse(date.concat(" ".concat(time)));

                            if (dateToBeSend != null) {

                                String stringDateToBeSend = formatter.format(dateToBeSend);
                                in.setText(stringDateToBeSend);
                            }

                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }, mHour, mMinute, false);
        timePickerDialog.show();

        //in.setText(formatter.format(new Date()));
    }

    public void getOutTime(View view) {

        // Get Current Time
        final Calendar c = Calendar.getInstance();
        int mHour = c.get(Calendar.HOUR_OF_DAY);
        int mMinute = c.get(Calendar.MINUTE);

        // Launch Time Picker Dialog
        TimePickerDialog timePickerDialog = new TimePickerDialog(this,
                new TimePickerDialog.OnTimeSetListener() {

                    @Override
                    public void onTimeSet(TimePicker view, int hourOfDay,
                                          int minute) {

                        String date = formatter2.format(new Date());
                        String time = String.valueOf(hourOfDay).concat(":").concat(String.valueOf(minute)).concat(":").concat("00");

                        try {
                            Date dateToBeSend = formatter.parse(date.concat(" ".concat(time)));

                            if (dateToBeSend != null) {

                                String stringDateToBeSend = formatter.format(dateToBeSend);
                                out.setText(stringDateToBeSend);
                            }

                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }, mHour, mMinute, false);
        timePickerDialog.show();

        //out.setText(formatter.format(new Date()));
    }

    public boolean isMobileNumberValid(String number){
        if(number.length() == 10){
            if (number.matches("[0-9]+")) {
                return true;
            }
        }
        return false;
    }

    public boolean isEmailValid(String email) {
        String expression = "^[\\w\\.-]+@([\\w\\-]+\\.)+[A-Z]{2,4}$";
        Pattern pattern = Pattern.compile(expression, Pattern.CASE_INSENSITIVE);
        Matcher matcher = pattern.matcher(email);
        return matcher.matches();
    }

    public void sendDataToServer() {

        Thread t1 = new Thread(new Runnable() {
            @Override
            public void run() {

                runOnUiThread(new Runnable() {
                    public void run() {
                        progDailog = new ProgressDialog(context);
                        progDailog.setMessage("Sending data to server...");
                        progDailog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
                        progDailog.setCancelable(false);
                        progDailog.setIndeterminate(false);
                        progDailog.show();
                    }
                });

                // Instantiate the RequestQueue.
                RequestQueue queue = Volley.newRequestQueue(context);
                String url ="http://3.6.107.191:8080/saveCustomerComplianceV0";

                // Request a string response from the provided URL.
                StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                        new Response.Listener<String>() {
                            @Override
                            public void onResponse(final String resp) {

                                runOnUiThread(new Runnable() {
                                    public void run() {

                                        if (progDailog != null) {
                                            progDailog.dismiss();
                                        }

                                        if (resp.equals("Success")) {

                                            new AlertDialog.Builder(context)
                                                    .setTitle("vProtect")
                                                    .setCancelable(false)
                                                    .setMessage("Data Successfully saved.")
                                                    .setPositiveButton("Okay", new DialogInterface.OnClickListener() {
                                                        @Override
                                                        public void onClick(DialogInterface dialog, int which) {
//                                                            Intent a = new Intent(AddCustomerComplianceActivity.this, BeaconActivity.class);
//                                                            a.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
//                                                            startActivity(a);

                                                            finish();

                                                        }
                                                    }).show();
                                        } else {

                                            new AlertDialog.Builder(context)
                                                    .setTitle("vProtect")
                                                    .setCancelable(false)
                                                    .setMessage("Something went wrong! Please try again later.")
                                                    .setPositiveButton("Okay", new DialogInterface.OnClickListener() {
                                                        @Override
                                                        public void onClick(DialogInterface dialog, int which) {
                                                            dialog.dismiss();
                                                        }
                                                    }).show();
                                        }
                                    }
                                });
                            }
                        }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        response = null;
                    }
                }){
                    @Override
                    public Map<String, String> getParams()
                    {
                        try {

                            int temperature = (int) Double.parseDouble(enteredTemp);

                            Map<String, String>  params = new HashMap<String, String>();
                            params.put("orgId", String.valueOf(orgId));
                            params.put("depId", String.valueOf(depId));
                            params.put("usrId", String.valueOf(usrId));
                            params.put("cusName", enteredName);
                            params.put("cusPhone", enteredPhone);
                            params.put("cusEmail", enteredEmail);
                            params.put("cusInTime", enteredIn);
                            params.put("cusOutTime", enteredOut);
                            params.put("cusPhotoUrl", s3Url);
                            params.put("cusTemperature", String.valueOf(temperature));
                            params.put("cusSelfStatus", selectedSelfStatus);
                            params.put("cusFamilyStatus", selectedFamilyStatus);
                            params.put("cusSaveDate", saveDate);
                            params.put("cusChecklistData", checklistData);

                            return params;
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        return null;
                    }
                };
                // Add the request to the RequestQueue.
                queue.add(stringRequest);

            }
        });
        t1.start();
    }

}
