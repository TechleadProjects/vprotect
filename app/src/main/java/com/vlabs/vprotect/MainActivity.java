package com.vlabs.vprotect;

import android.Manifest;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothManager;
import android.bluetooth.le.ScanCallback;
import android.bluetooth.le.ScanResult;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Vibrator;
import android.speech.tts.TextToSpeech;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

public class MainActivity extends AppCompatActivity {

    private Context context;
    private BluetoothAdapter bluetoothAdapter;
    private String TAG = MainActivity.class.toString();
    private static final int PERMISSION_REQUEST_COARSE_LOCATION = 456;

    private Button btnBleSearch;
    private Button bleStopSearch;

    private TextView textBleData;
    private TextView textMyBlId;

    private Map<String, Integer> deviceMap = new HashMap<>();

    private String bleData = "";

    boolean bluetoothSwitchedOFF = false;

    private List<BluetoothDevice> mDeviceList = new ArrayList<>();

    private TextToSpeech t1;

    private Vibrator v;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // Get instance of Vibrator from current Context
        v = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);

        IntentFilter filter = new IntentFilter();
        filter.addAction(BluetoothAdapter.ACTION_STATE_CHANGED);
        filter.addAction(BluetoothDevice.ACTION_FOUND);
        filter.addAction(BluetoothAdapter.ACTION_DISCOVERY_STARTED);
        filter.addAction(BluetoothAdapter.ACTION_DISCOVERY_FINISHED);
        registerReceiver(mReceiver, filter);

        context = this;

        try{
            t1=new TextToSpeech(getApplicationContext(), new TextToSpeech.OnInitListener() {
                @Override
                public void onInit(int status) {
                    if(status != TextToSpeech.ERROR) {
                        t1.setLanguage(Locale.UK);
                        t1.speak("Hello Sagar", TextToSpeech.QUEUE_FLUSH, null,null);
                    }
                }
            });


        }catch (Exception e) {
            e.printStackTrace();
        }

        btnBleSearch = (Button)findViewById(R.id.bleSearch);
        bleStopSearch = (Button)findViewById(R.id.bleStopSearch);

        textBleData = (TextView)findViewById(R.id.textBleData);
        textMyBlId = (TextView)findViewById(R.id.textMyBlId);


        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            requestPermissions(new String[]{Manifest.permission.ACCESS_COARSE_LOCATION}, PERMISSION_REQUEST_COARSE_LOCATION);
        }

        BluetoothManager bluetoothManager = (BluetoothManager) context.getSystemService(Context.BLUETOOTH_SERVICE);
        bluetoothAdapter = bluetoothManager.getAdapter();

        btnBleSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                final String bleAddress = getBluetoothMacAddress();

                deviceMap.clear();

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        textMyBlId.setText(bleAddress);
                    }
                });

                   bluetoothAdapter.startDiscovery();
//                bluetoothAdapter.getBluetoothLeScanner().startScan(new ScanCallback() {
//                    @Override
//                    public void onScanResult(int callbackType, ScanResult result) {
//                        super.onScanResult(callbackType, result);
//                        BluetoothDevice bd = result.getDevice();
//                        final String address = bd.getAddress();
//                        final String name = bd.getName();
//                        final int type = bd.getType();
//                        final String blClass = bd.getBluetoothClass().toString();
//                        final int range = result.getRssi();
//
//                        runOnUiThread(new Runnable() {
//                            @Override
//                            public void run() {
//                                textBleData.setText(address + " " + name + " " + String.valueOf(type) + " " + String.valueOf(range) + "\n" + textBleData.getText() + "\n" );
//                            }
//                        });
//
//
//                        Log.i(TAG, "Remote device name: " + result.getDevice().getName());
//                    }
//                });
            }
        });

        bleStopSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                bluetoothAdapter.getBluetoothLeScanner().stopScan(new ScanCallback() {
                    @Override
                    public void onScanResult(int callbackType, ScanResult result) {
                        super.onScanResult(callbackType, result);
                    }

                    @Override
                    public void onBatchScanResults(List<ScanResult> results) {
                        super.onBatchScanResults(results);
                    }

                    @Override
                    public void onScanFailed(int errorCode) {
                        super.onScanFailed(errorCode);
                    }
                });

            }
        });

    }

    private String getBluetoothMacAddress() {
        BluetoothAdapter bluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        String bluetoothMacAddress = "";
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M){
            try {
                Field mServiceField = bluetoothAdapter.getClass().getDeclaredField("mService");
                mServiceField.setAccessible(true);

                Object btManagerService = mServiceField.get(bluetoothAdapter);

                if (btManagerService != null) {
                    bluetoothMacAddress = (String) btManagerService.getClass().getMethod("getAddress").invoke(btManagerService);
                }
            } catch (NoSuchFieldException e) {

            } catch (NoSuchMethodException e) {

            } catch (IllegalAccessException e) {

            } catch (InvocationTargetException e) {

            }
        } else {
            bluetoothMacAddress = bluetoothAdapter.getAddress();
        }
        return bluetoothMacAddress;
    }

    private final BroadcastReceiver mReceiver = new BroadcastReceiver() {
        public void onReceive(final Context context, Intent intent) {
            String action = intent.getAction();

            if (BluetoothAdapter.ACTION_STATE_CHANGED.equals(action)) {
                final int state = intent.getIntExtra(BluetoothAdapter.EXTRA_STATE, BluetoothAdapter.ERROR);

                if (state == BluetoothAdapter.STATE_ON) {
                    //showToast("ACTION_STATE_CHANGED: STATE_ON");
                }
            }

            else if (BluetoothAdapter.ACTION_DISCOVERY_STARTED.equals(action)) {
                //mDeviceList = new ArrayList<>();
                //showToast("ACTION_DISCOVERY_STARTED");
                //mProgressDlg.show();
            }

            else if (BluetoothAdapter.ACTION_DISCOVERY_FINISHED.equals(action) && !bluetoothSwitchedOFF) {
                //mProgressDlg.dismiss();
                //showToast("ACTION_DISCOVERY_FINISHED");

                //Intent newIntent = new Intent(MainActivity.this, MainActivity.class);

                //newIntent.putParcelableArrayListExtra("device.list", mDeviceList);

                //startActivity(newIntent);
                deviceMap.clear();
                bluetoothAdapter.startDiscovery();
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(context,"Restarting the search!",Toast.LENGTH_LONG).show();
                    }
                });
            }

            else if (BluetoothDevice.ACTION_FOUND.equals(action)) {// When discovery finds a device
                // Get the BluetoothDevice object from the Intent

                int  rssi = intent.getShortExtra(BluetoothDevice.EXTRA_RSSI,Short.MIN_VALUE);
                BluetoothDevice device = intent.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE);
                mDeviceList.add(device);
                deviceMap.put(device.getAddress() + " " + device.getName(),rssi);
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {

                        StringBuilder sb = new StringBuilder();
                        // using for-each loop for iteration over Map.entrySet()
                        for (Map.Entry<String,Integer> entry : deviceMap.entrySet()) {

                            int count = 0;
                            String msg = "";
                            if(entry.getValue() > -55)
                            {
                                count++;
                                msg = "Social Distancing Needed! Please move away!";
                                sb.append(entry.getKey() + "  " + msg + "\n\n");
                            }

                            if(count>0)
                            {
                                if(t1.isSpeaking() ==false) {
                                    t1.speak("CORONA Alert! Please move away and maintain Social Distancing! ", TextToSpeech.QUEUE_FLUSH, null, null);
                                    // Vibrate for 400 milliseconds
                                    v.vibrate(1000);

                                }
                            }


                        }
                        textBleData.setText(sb.toString());
                    }
                });
                //showToast("Device found = " + device.getName());
            }
        }
    };

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String permissions[], @NonNull int[] grantResults) {
        switch (requestCode) {
            case PERMISSION_REQUEST_COARSE_LOCATION: {
                try {
                    if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                        // Permission granted, yay! Start the Bluetooth device scan.
                    } else {
                        // Alert the user that this application requires the location permission to perform the scan.
                    }
                }catch(Exception e)
                {
                    e.printStackTrace();
                }
            }
        }
    }
}
