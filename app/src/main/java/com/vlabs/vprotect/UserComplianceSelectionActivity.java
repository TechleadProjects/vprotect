package com.vlabs.vprotect;

import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.widget.DatePicker;
import android.widget.LinearLayout;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.gson.Gson;
import com.vlabs.vprotect.Adapters.UserComplianceRecyclerAdapter;
import com.vlabs.vprotect.Util.Util;
import com.vlabs.vprotect.model.AppUser;
import com.vlabs.vprotect.model.UserComplianceData;

import org.json.JSONArray;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

public class UserComplianceSelectionActivity extends AppCompatActivity {

    private Context context;
    private Util util;

    private int orgId, depId, usrId;

    private String response, selectedDate;
    private static RecyclerView.Adapter adapter;
    private RecyclerView.LayoutManager layoutManager;
    private static RecyclerView recyclerView;
    private static ArrayList<UserComplianceData> complianceDataArrayList;
    private ProgressDialog progDailog;
    private LinearLayout layNoRecord, layNoInternet;
    private LinearLayout layParent;
    private FloatingActionButton addCustomerCompliance;
    private SimpleDateFormat formatter;
    private AppUser appUser;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_compliance_selection);

        context= this;
        util = new Util();
        complianceDataArrayList = new ArrayList<>();
        formatter = new SimpleDateFormat("yyyy-MM-dd");

        Gson gson = new Gson();
        SharedPreferences pref = this.getSharedPreferences("LoginData", 0); // 0 - for private mode
        String loggedinuser  = pref.getString("loggedinuser","");
        appUser = gson.fromJson(loggedinuser, AppUser.class);

        orgId = appUser.orgId;
        depId = appUser.depId;
        usrId = appUser.usrId;

        layParent = findViewById(R.id.layParent);
        layNoRecord = findViewById(R.id.layNoRecord);
        layNoInternet = findViewById(R.id.layNoInternet);

        recyclerView = findViewById(R.id.issued_books_recycler_view);
        recyclerView.setHasFixedSize(true);

        layoutManager = new LinearLayoutManager(context);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());

        final Calendar myCalendar = Calendar.getInstance();

        DatePickerDialog.OnDateSetListener date = new DatePickerDialog.OnDateSetListener() {

            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear,
                                  int dayOfMonth) {
                // TODO Auto-generated method stub
                myCalendar.set(Calendar.YEAR, year);
                myCalendar.set(Calendar.MONTH, monthOfYear);
                myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);

                selectedDate = formatter.format(myCalendar.getTime());

                new GetUserCompliances().execute();

            }

        };

        // TODO Auto-generated method stub
        DatePickerDialog datePickerDialog = new DatePickerDialog(context, date, myCalendar
                .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                myCalendar.get(Calendar.DAY_OF_MONTH));
        datePickerDialog.setCanceledOnTouchOutside(false);
        datePickerDialog.setCancelable(false);
        datePickerDialog.show();

        datePickerDialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialogInterface) {
                finish();
            }
        });

        //selectedDate = formatter.format(new Date());
    }

    public void goBack(View view) {

        finish();
    }

    public class GetUserCompliances extends AsyncTask<String, String, String> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progDailog = new ProgressDialog(context);
            progDailog.setMessage("Loading...");
            progDailog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            progDailog.setCancelable(false);
            progDailog.setIndeterminate(false);
            progDailog.show();
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);

            try {
                if (progDailog != null) {
                    progDailog.dismiss();
                }
                if (response != null) {
                    JSONArray array = new JSONArray(response);

                    complianceDataArrayList = new ArrayList<>();

                    if(array.length() > 0) {
                        for (int i=0; i<array.length(); i++) {

                            JSONObject jsonObject = array.getJSONObject(i);

                            UserComplianceData userComplianceData = new UserComplianceData();
                            userComplianceData.setUscName(jsonObject.getString("uscName"));
                            userComplianceData.setChkId(jsonObject.getInt("chkId"));
                            userComplianceData.setUscId(jsonObject.getInt("uscId"));
                            userComplianceData.setChkName(jsonObject.getString("chkName"));
                            userComplianceData.setChkQuestionJson(jsonObject.getString("chkQuestionJson"));
                            userComplianceData.setUscAnswerJson(jsonObject.getString("uscAnswerJson"));
                            userComplianceData.setUscAddedDate(jsonObject.getString("uscAddedDate"));
                            userComplianceData.setUscScheduledEndDate(jsonObject.getString("uscScheduledEndDate"));
                            userComplianceData.setUscActualEndDate(jsonObject.getString("uscActualEndDate"));
                            userComplianceData.setUscStatus(jsonObject.getString("uscStatus"));
                            userComplianceData.setUscComment(jsonObject.getString("uscComment"));
                            userComplianceData.setUscPhotoUrl(jsonObject.getString("uscPhotoUrl"));

                            complianceDataArrayList.add(userComplianceData);

                        }

                        if(complianceDataArrayList.size() > 0) {

                            layParent.setVisibility(View.VISIBLE);
                            adapter = new UserComplianceRecyclerAdapter(context, complianceDataArrayList);
                            recyclerView.setAdapter(adapter);
                        }
                    } else {

                        layParent.setVisibility(View.GONE);
                        layNoRecord.setVisibility(View.VISIBLE);
                    }

                } else {
                    Toast.makeText(context, "Couldn't reach servers at the moment. Please try again later.", Toast.LENGTH_SHORT).show();
                    finish();
                }

            } catch (Exception e) {
                e.printStackTrace();

                layParent.setVisibility(View.GONE);
                layNoRecord.setVisibility(View.VISIBLE);
            }
        }

        @Override
        protected String doInBackground(String... strings) {
            try {

                response = util.getUserCompliances(String.valueOf(orgId), String.valueOf(depId), String.valueOf(usrId), String.valueOf(selectedDate), "A");

            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }
    }

    @Override
    protected void onRestart() {
        super.onRestart();

        finish();
        startActivity(getIntent());
    }
}
