package com.vlabs.vprotect.Util;

import java.net.URLEncoder;

public class Util {
    private String strMainUrl;

    public Util() {
        strMainUrl = "http://3.6.107.191:8080";
    }

    public String getCustomerCompliances(String orgId, String depId, String usrId, String date) {

        try {
            StringBuilder sb = new StringBuilder();
            sb.append(strMainUrl);
            sb.append("/getCustomerCompliances?");
            sb.append("orgId=").append(orgId);
            sb.append("&depId=").append(depId);
            sb.append("&usrId=").append(usrId);
            sb.append("&selectedDate=").append(URLEncoder.encode(date, "UTF-8"));

            WebService WebService = new WebService(sb.toString());
            WebService.start();
            try {
                WebService.join(30000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

            return WebService.output.toString();
        }catch (Exception e){
            e.printStackTrace();
        }
        return null;
    }

    public String getUserCompliances(String orgId, String depId, String usrId, String date, String status) {

        try {
            StringBuilder sb = new StringBuilder();
            sb.append(strMainUrl);
            sb.append("/getUserCompliances?");
            sb.append("orgId=").append(orgId);
            sb.append("&depId=").append(depId);
            sb.append("&usrId=").append(usrId);
            sb.append("&selectedDate=").append(URLEncoder.encode(date, "UTF-8"));
            sb.append("&status=").append(status);

            WebService WebService = new WebService(sb.toString());
            WebService.start();
            try {
                WebService.join(30000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

            return WebService.output.toString();
        }catch (Exception e){
            e.printStackTrace();
        }
        return null;
    }

    public String getCirculars(String usrId, String status) {

        try {
            StringBuilder sb = new StringBuilder();
            sb.append(strMainUrl);
            sb.append("/getCirculars?");
            sb.append("usrId=").append(usrId);
            sb.append("&cirStatus=").append(status);

            WebService WebService = new WebService(sb.toString());
            WebService.start();
            try {
                WebService.join(30000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

            return WebService.output.toString();
        }catch (Exception e){
            e.printStackTrace();
        }
        return null;
    }

    public String getUser(String usrId) {

        try {
            StringBuilder sb = new StringBuilder();
            sb.append(strMainUrl);
            sb.append("/getUser?");
            sb.append("usrId=").append(usrId);

            WebService WebService = new WebService(sb.toString());
            WebService.start();
            try {
                WebService.join(30000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

            return WebService.output.toString();
        }catch (Exception e){
            e.printStackTrace();
        }
        return null;
    }

    public String getAwsS3Credentials() {

        try {
            StringBuilder sb = new StringBuilder();
            sb.append("http://etechschoolonline.com/webresources/");
            sb.append("institute/getAwsS3Credentials?");

            WebService WebService = new WebService(sb.toString());
            WebService.start();
            try {
                WebService.join(30000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

            return WebService.output.toString();
        }catch (Exception e){
            e.printStackTrace();
        }
        return null;
    }

    public String getBucketName() {
        return "vprotectbucket";
    }
}
