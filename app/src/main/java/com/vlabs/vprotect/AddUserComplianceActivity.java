package com.vlabs.vprotect;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.amazonaws.ClientConfiguration;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.regions.Region;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3Client;
import com.amazonaws.services.s3.model.CannedAccessControlList;
import com.amazonaws.services.s3.model.PutObjectRequest;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.github.dhaval2404.imagepicker.ImagePicker;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.vlabs.vprotect.Adapters.QuestionListRecyclerAdapter;
import com.vlabs.vprotect.Util.Util;
import com.vlabs.vprotect.model.AppUser;
import com.vlabs.vprotect.model.Question;
import com.vlabs.vprotect.model.UserComplianceData;

import java.io.File;
import java.lang.reflect.Type;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class AddUserComplianceActivity extends AppCompatActivity {

    private Context context;
    private Util util;

    private int orgId, depId, usrId;

    private ArrayList<Question> questionArrayList;
    private LinearLayout layNoRecord;
    private RelativeLayout layParent;
    private TextView checklistName;
    private EditText comment;
    private ProgressDialog progDailog;

    private static RecyclerView.Adapter adapter;
    private RecyclerView.LayoutManager layoutManager;
    private static RecyclerView recyclerView;

    private FloatingActionButton saveUserCompliance;

    private UserComplianceData userComplianceData;
    private String uscStatus, uscComment, uscAnswerJson;
    private AppUser appUser;
    private SimpleDateFormat formatter1, formatter2;
    private String accessKey, secretKey;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_user_compliance);

        context = this;
        util = new Util();
        formatter1 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        formatter2 = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS");

        Gson gson = new Gson();
        SharedPreferences pref = this.getSharedPreferences("LoginData", 0); // 0 - for private mode
        String loggedinuser  = pref.getString("loggedinuser","");
        appUser = gson.fromJson(loggedinuser, AppUser.class);

        orgId = appUser.orgId;
        depId = appUser.depId;
        usrId = appUser.usrId;

        final SharedPreferences sharedPreferences = getSharedPreferences("user", 0);

        accessKey = sharedPreferences.getString("accessKey", null);
        secretKey = sharedPreferences.getString("secretKey", null);

        layParent = findViewById(R.id.layParent);
        layNoRecord = findViewById(R.id.layNoRecord);
        checklistName = findViewById(R.id.checklistName);

        saveUserCompliance = findViewById(R.id.saveUserCompliance);
        comment = findViewById(R.id.comment);

        recyclerView = findViewById(R.id.issued_books_recycler_view);
        recyclerView.setHasFixedSize(true);

        layoutManager = new LinearLayoutManager(context);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());

        recyclerView.addItemDecoration(new DividerItemDecoration(context, DividerItemDecoration.VERTICAL));

        String questionsArrayListString = getIntent().getStringExtra("questionsArrayListString");
        String userComplianceDataString = getIntent().getStringExtra("checkListObject");

        //get
        Type type = new TypeToken<List<Question>>(){}.getType();
        questionArrayList = gson.fromJson(questionsArrayListString, type);

        Type type2 = new TypeToken<UserComplianceData>(){}.getType();
        userComplianceData = gson.fromJson(userComplianceDataString, type2);

        if(userComplianceData != null) {
            checklistName.setText(userComplianceData.getChkName());
        }

        if(questionArrayList != null && questionArrayList.size() > 0) {

            layParent.setVisibility(View.VISIBLE);
            layNoRecord.setVisibility(View.GONE);

            adapter = new QuestionListRecyclerAdapter(context, questionArrayList, appUser, userComplianceData);
            recyclerView.setAdapter(adapter);

        } else {

            layParent.setVisibility(View.GONE);
            layNoRecord.setVisibility(View.VISIBLE);

        }

        saveUserCompliance.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                uscComment = comment.getText().toString();

                if (uscComment == null || uscComment.equals("")) {
                    Toast.makeText(context, "Please enter comment!", Toast.LENGTH_SHORT).show();
                    return;
                }

                new AlertDialog.Builder(context)
                        .setTitle("Student Connect")
                        .setMessage("Are you sure you want to submit?")
                        .setPositiveButton("Submit", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {

                                ArrayList<Question> answers = ((QuestionListRecyclerAdapter) adapter).getAnsSet();
                                final ArrayList<File> files = ((QuestionListRecyclerAdapter) adapter).getFilesToBeUpload();
                                final ArrayList<String> fileNames = ((QuestionListRecyclerAdapter) adapter).getFileNames();
                                int count = 0;

                                for (int i=0; i<answers.size(); i++) {

                                    if(!answers.get(i).getqAnswer().equals("")) {
                                        count++;
                                    }
                                }

                                if(count == answers.size()) {
                                    uscStatus = "C";
                                } else {
                                    uscStatus = "H";
                                }

                                Gson gson = new Gson();
                                uscAnswerJson = gson.toJson(answers);

                                Thread t1 = new Thread(new Runnable() {
                                    @Override
                                    public void run() {

                                        runOnUiThread(new Runnable() {
                                            public void run() {
                                                progDailog = new ProgressDialog(context);
                                                progDailog.setMessage("Sending data to server...");
                                                progDailog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
                                                progDailog.setCancelable(false);
                                                progDailog.setIndeterminate(false);
                                                progDailog.show();
                                            }
                                        });

                                        try {

                                            int j = 0;

                                            for (int i=0; i<files.size(); i++) {

                                                boolean status = uploadFilesToS3(files.get(i).getPath(), fileNames.get(i));

                                                if (status) {
                                                    j++;
                                                } else {
                                                    break;
                                                }

                                            }

                                            if (j == files.size()) {

                                                // Instantiate the RequestQueue.
                                                RequestQueue queue = Volley.newRequestQueue(context);
                                                String url ="http://3.6.107.191:8080/saveUserCompliance";

                                                // Request a string response from the provided URL.
                                                StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                                                        new Response.Listener<String>() {
                                                            @Override
                                                            public void onResponse(final String resp) {

                                                                runOnUiThread(new Runnable() {
                                                                    public void run() {

                                                                        if (progDailog != null) {
                                                                            progDailog.dismiss();
                                                                        }

                                                                        if (resp.equals("Success")) {

                                                                            new AlertDialog.Builder(context)
                                                                                    .setTitle("vProtect")
                                                                                    .setCancelable(false)
                                                                                    .setMessage("Data Successfully saved.")
                                                                                    .setPositiveButton("Okay", new DialogInterface.OnClickListener() {
                                                                                        @Override
                                                                                        public void onClick(DialogInterface dialog, int which) {
//                                                                    Intent a = new Intent(AddUserComplianceActivity.this, BeaconActivity.class);
//                                                                    a.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
//                                                                    startActivity(a);

                                                                                            finish();

                                                                                        }
                                                                                    }).show();
                                                                        } else {

                                                                            new AlertDialog.Builder(context)
                                                                                    .setTitle("vProtect")
                                                                                    .setCancelable(false)
                                                                                    .setMessage("Something went wrong! Please try again later.")
                                                                                    .setPositiveButton("Okay", new DialogInterface.OnClickListener() {
                                                                                        @Override
                                                                                        public void onClick(DialogInterface dialog, int which) {
                                                                                            dialog.dismiss();
                                                                                        }
                                                                                    }).show();
                                                                        }
                                                                    }
                                                                });
                                                            }
                                                        }, new Response.ErrorListener() {
                                                    @Override
                                                    public void onErrorResponse(VolleyError error) {

                                                        if (progDailog != null) {
                                                            progDailog.dismiss();
                                                        }

                                                        new AlertDialog.Builder(context)
                                                                .setTitle("vProtect")
                                                                .setCancelable(false)
                                                                .setMessage("Something went wrong! Please try again later.")
                                                                .setPositiveButton("Okay", new DialogInterface.OnClickListener() {
                                                                    @Override
                                                                    public void onClick(DialogInterface dialog, int which) {
                                                                        dialog.dismiss();
                                                                    }
                                                                }).show();
                                                    }
                                                }){
                                                    @Override
                                                    public Map<String, String> getParams()
                                                    {
                                                        try {

                                                            Map<String, String> params = new HashMap<String, String>();
                                                            params.put("orgId", String.valueOf(orgId));
                                                            params.put("depId", String.valueOf(depId));
                                                            params.put("usrId", String.valueOf(usrId));
                                                            params.put("uscId", String.valueOf(userComplianceData.getUscId()));
                                                            params.put("uscAddedDate", !userComplianceData.getUscAddedDate().equals("null") ? formatter1.format(formatter2.parse(userComplianceData.getUscAddedDate())) : "null");
                                                            params.put("uscName", userComplianceData.getUscName());
                                                            params.put("chkId", String.valueOf(userComplianceData.getChkId()));
                                                            params.put("chkName", userComplianceData.getChkName());
                                                            params.put("chkQuestionJson", userComplianceData.getChkQuestionJson());
                                                            params.put("uscAnswerJson", uscAnswerJson);
                                                            params.put("uscScheduledEndDate", !userComplianceData.getUscScheduledEndDate().equals("null") ? formatter1.format(formatter2.parse(userComplianceData.getUscScheduledEndDate())) : "null");
                                                            params.put("uscActualEndDate", !userComplianceData.getUscActualEndDate().equals("null") ? formatter1.format(formatter2.parse(userComplianceData.getUscActualEndDate())) : formatter1.format(new Date()));
                                                            params.put("uscStatus", uscStatus);
                                                            params.put("uscComment", uscComment);
                                                            params.put("uscPhotoUrl", userComplianceData.getUscPhotoUrl());

                                                            return params;
                                                        } catch (Exception e) {
                                                            e.printStackTrace();
                                                        }
                                                        return null;
                                                    }
                                                };
                                                // Add the request to the RequestQueue.
                                                queue.add(stringRequest);

                                            } else {

                                                new AlertDialog.Builder(context)
                                                        .setTitle("vProtect")
                                                        .setCancelable(false)
                                                        .setMessage("Failed to upload files on server. Please try again.")
                                                        .setPositiveButton("Okay", new DialogInterface.OnClickListener() {
                                                            @Override
                                                            public void onClick(DialogInterface dialog, int which) {
                                                                dialog.dismiss();
                                                            }
                                                        }).show();
                                            }

                                        } catch (Exception e) {
                                            e.printStackTrace();
                                        }

                                    }
                                });
                                t1.start();

                            }
                        })
                        .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        }).show();

            }
        });

    }

    public void goBack(View view) {

        finish();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == Activity.RESULT_OK) {

            //profilePhoto.setImageURI(data.getData());

            //You can get File object from intent
            File file = ImagePicker.Companion.getFile(data);
            ((QuestionListRecyclerAdapter) adapter).setSelectedFile(file);

//            if(file != null) {
//                profileByteString = file.getPath();
//
//                String[] pathArray = profileByteString.split("/");
//                displayName = pathArray[pathArray.length - 1];
//
//            } else {
//                Toast.makeText(this, "Could not select Image.", Toast.LENGTH_SHORT).show();
//            }

        } else if (resultCode == ImagePicker.RESULT_ERROR) {
            Toast.makeText(this, "Could not select Image.", Toast.LENGTH_SHORT).show();
        }
    }

    public boolean uploadFilesToS3(String filePath, String fileName){

        try {

            ClientConfiguration clientConfiguration = new ClientConfiguration()
                    .withMaxErrorRetry(1) // 1 retries
                    .withConnectionTimeout(200000) // 30,000 ms
                    .withSocketTimeout(200000); // 30,000 ms

            AmazonS3 s3Client = new AmazonS3Client(new BasicAWSCredentials(accessKey, secretKey), clientConfiguration);
            s3Client.setRegion(Region.getRegion(Regions.AP_SOUTHEAST_1));

            PutObjectRequest por = new PutObjectRequest(util.getBucketName(), fileName, new File(filePath));
            por.withCannedAcl(CannedAccessControlList.PublicRead);
            s3Client.putObject(por);
            return true;
        } catch (Exception e){
            return false;
        }
    }

}
