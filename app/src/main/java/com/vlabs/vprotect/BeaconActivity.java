package com.vlabs.vprotect;

import android.Manifest;
import android.app.ActionBar;
import android.app.Activity;
import android.app.ActivityManager;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.bluetooth.BluetoothAdapter;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import com.airbnb.lottie.LottieAnimationView;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;
import com.vlabs.vprotect.Adapters.CircularsRecyclerAdapter;
import com.vlabs.vprotect.Util.Util;
import com.vlabs.vprotect.model.AppUser;
import com.vlabs.vprotect.model.Circular;

import org.json.JSONArray;
import org.json.JSONObject;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;


public class BeaconActivity extends AppCompatActivity {

    private Button btnStartService;
    private Button btnStopService;
    private Button btnLogout;

    private AppUser appUser;

    private LottieAnimationView animationView;
    private ProgressDialog progDailog;
    private String response;

    private static final int PERMISSION_REQUEST_COARSE_LOCATION = 456;

    private Context context;
    private Util util;
    private int usrId;
    int pageIndex = 0;
    private String macAddress;
    private BluetoothAdapter mBluetoothAdapter;
    private int REQUEST_ENABLE_BT = 1111;
    private TextView version;

    public static boolean isDebugMode = false;

    private BroadcastReceiver mMessageReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            // Get extra data included in the Intent
            String message = intent.getStringExtra("Status");

            if (message != null && message.equals("Danger")) {
                animationView.setProgress(0);
                animationView.setAnimation(R.raw.keepdistancecoronavirus);
                animationView.playAnimation();
            } else {
                animationView.setProgress(0);
                animationView.setAnimation(R.raw.noproblem);
                animationView.playAnimation();
            }
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_beacon);

//        Intent discoverableIntent =
//                new Intent(BluetoothAdapter.ACTION_REQUEST_DISCOVERABLE);
//        discoverableIntent.putExtra(BluetoothAdapter.EXTRA_DISCOVERABLE_DURATION, 7200);
//        startActivity(discoverableIntent);

        Gson gson = new Gson();
        SharedPreferences pref = getApplicationContext().getSharedPreferences("LoginData", 0); // 0 - for private mode
        String loggedinuser = pref.getString("loggedinuser", "");
        appUser = gson.fromJson(loggedinuser, AppUser.class);

        usrId = appUser.usrId;

        context = this;
        util = new Util();

        btnStartService = (Button) findViewById(R.id.btnStartService);
        btnStopService = (Button) findViewById(R.id.btnStopService);
        btnLogout = (Button) findViewById(R.id.btnLogout);
        version = findViewById(R.id.version);

        try {
            PackageInfo pInfo = context.getPackageManager().getPackageInfo(getPackageName(), 0);
            version.setText(pInfo.versionName);
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }

        animationView = findViewById(R.id.animationView);

        //pk
        mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        if (!mBluetoothAdapter.isEnabled()) {
            Intent enableBtIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
            startActivityForResult(enableBtIntent, REQUEST_ENABLE_BT);
        } else {
            //Toast.makeText(this,mBluetoothAdapter.getName(),Toast.LENGTH_LONG).show();
            try {
                mBluetoothAdapter.setName(appUser.usrId + "#" + appUser.usrFullName);
            } catch (Exception e) {
                e.printStackTrace();
            }

        }

        btnStartService.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startService(new Intent(context, BGService.class));
            }
        });

        btnStopService.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                stopService(new Intent(context, BGService.class));
            }
        });

        btnLogout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                logout();
            }
        });

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            requestPermissions(new String[]{Manifest.permission.ACCESS_COARSE_LOCATION}, PERMISSION_REQUEST_COARSE_LOCATION);
        } else {

        }

        LocalBroadcastManager.getInstance(this).registerReceiver(
                mMessageReceiver, new IntentFilter("Distancing"));

        if (isMyServiceRunning(BeaconActivity.this, BGService.class)) { // Service class name
            Toast.makeText(context, "vProtect Service is already running!", Toast.LENGTH_LONG).show();
        } else {
            if (mBluetoothAdapter.isEnabled()) {
                startService(new Intent(context, BGService.class));
            }

        }

        sendBleId();

        // Check if Bluetooth is in discoverable mode or not


//        Toast.makeText(this,"ScanMode " + mBluetoothAdapter.getScanMode(),Toast.LENGTH_LONG).show();

        if (mBluetoothAdapter.getScanMode()!=BluetoothAdapter.SCAN_MODE_CONNECTABLE_DISCOVERABLE) {

            //Toast.makeText(this,"Discovery is off!",Toast.LENGTH_LONG).show();
            Intent dIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_DISCOVERABLE);
            dIntent.putExtra(BluetoothAdapter.EXTRA_DISCOVERABLE_DURATION, 3600);
            startActivity(dIntent);
        }
        else
        {
            //Toast.makeText(this,"Discovery has started!",Toast.LENGTH_LONG).show();
        }


    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        super.onActivityResult(requestCode, resultCode, data);
        // check if the request code is same as what is passed  here it is 2
        if(requestCode==REQUEST_ENABLE_BT) {
            try {
                mBluetoothAdapter.setName(appUser.usrId+"#"+appUser.usrFullName);
            } catch (Exception e)
            {

            }

            //Toast.makeText(this,"result Code " + resultCode,Toast.LENGTH_LONG).show();
            if (isMyServiceRunning(BeaconActivity.this, BGService.class)) { // Service class name
                Toast.makeText(context,"vProtect Service is already running!",Toast.LENGTH_LONG).show();
            } else {
                startService(new Intent(context, BGService.class));
            }
        }
    }

    private String getBluetoothMacAddress() {
        BluetoothAdapter bluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        String bluetoothMacAddress = "";
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M){
            try {
                Field mServiceField = bluetoothAdapter.getClass().getDeclaredField("mService");
                mServiceField.setAccessible(true);

                Object btManagerService = mServiceField.get(bluetoothAdapter);

                if (btManagerService != null) {
                    bluetoothMacAddress = (String) btManagerService.getClass().getMethod("getAddress").invoke(btManagerService);
                }
            } catch (NoSuchFieldException e) {

            } catch (NoSuchMethodException e) {

            } catch (IllegalAccessException e) {

            } catch (InvocationTargetException e) {

            }
        } else {
            bluetoothMacAddress = bluetoothAdapter.getAddress();
        }
        return bluetoothMacAddress;
    }

    private void sendBleId() {

        macAddress = getBluetoothMacAddress();
        //final String address = "Address";

        if(macAddress.isEmpty())
        {
            //Toast.makeText(context,"Unable to fetch Bluetooth ID!",Toast.LENGTH_LONG).show();
            macAddress = "UNKNOWN";

        }

        RequestQueue queue = Volley.newRequestQueue(this);
        String url ="http://3.6.107.191:8080/setUsrBluId";

        StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        //Toast.makeText(context,response,Toast.LENGTH_LONG).show();

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                //Toast.makeText(context,error.getMessage(),Toast.LENGTH_LONG).show();
            }
        }){
            @Override
            public Map<String, String> getParams()
            {
                Map<String, String>  params = new HashMap<String, String>();
                params.put("usrId", appUser.usrId.toString());
                params.put("usrBluId",macAddress);

                return params;
            }
        };
        // Add the request to the RequestQueue.
        queue.add(stringRequest);
    }

    public static boolean isMyServiceRunning(Activity activity, Class<?> serviceClass) {
        ActivityManager manager = (ActivityManager) activity.getSystemService(Context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if (serviceClass.getName().equals(service.service.getClassName())) {
                return true;
            }
        }
        return false;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String permissions[], @NonNull int[] grantResults) {
        switch (requestCode) {
            case PERMISSION_REQUEST_COARSE_LOCATION: {
                try {
                    if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                        // Permission granted, yay! Start the Bluetooth device scan.
                    } else {
                        // Alert the user that this application requires the location permission to perform the scan.
                    }
                }catch(Exception e)
                {
                    e.printStackTrace();
                }
            }
        }
    }


    public void openMenu(View view) {

        final Dialog dialog = new Dialog(context);
        dialog.setContentView(R.layout.dialog_menu);
        dialog.setCancelable(true);
        dialog.show();
        Window window = dialog.getWindow();
        window.setLayout(ActionBar.LayoutParams.WRAP_CONTENT, ActionBar.LayoutParams.WRAP_CONTENT);

        ImageView closeDialog = dialog.findViewById(R.id.closeDialog);
        TextView virus = dialog.findViewById(R.id.virus);
        TextView endService = dialog.findViewById(R.id.endService);
        TextView notifications = dialog.findViewById(R.id.notifications);
        TextView checklists = dialog.findViewById(R.id.checklists);
        TextView customerCompliance = dialog.findViewById(R.id.customerCompliance);
        TextView userProfile = dialog.findViewById(R.id.userProfile);
        TextView logout = dialog.findViewById(R.id.logout);

        closeDialog.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });

        notifications.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();

                new androidx.appcompat.app.AlertDialog.Builder(context)
                        .setTitle("vProtect")
                        .setCancelable(false)
                        .setMessage("Coming Soon!")
                        .setPositiveButton("Okay", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        }).show();
            }
        });

        checklists.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();

                new androidx.appcompat.app.AlertDialog.Builder(context)
                        .setTitle("vProtect")
                        .setCancelable(false)
                        .setMessage("Coming Soon!")
                        .setPositiveButton("Okay", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        }).show();
            }
        });

        customerCompliance.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();

                Intent intent = new Intent(BeaconActivity.this, CustomerComplianceSelectionActivity.class);
                startActivity(intent);
            }
        });

        userProfile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();

                Intent intent = new Intent(BeaconActivity.this, UserProfileActivity.class);
                startActivity(intent);
            }
        });

        endService.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();

                stopService(new Intent(context, BGService.class));

                animationView.setProgress(0);
                animationView.setAnimation(R.raw.wrong);
                animationView.playAnimation();
            }
        });

        virus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();

                final Dialog dialog = new Dialog(context);
                dialog.setContentView(R.layout.dialog_health_awareness);
                dialog.setCancelable(true);
                dialog.show();
                Window window = dialog.getWindow();
                window.setLayout(ActionBar.LayoutParams.WRAP_CONTENT, ActionBar.LayoutParams.WRAP_CONTENT);

                window.setBackgroundDrawable(ContextCompat.getDrawable(context, R.drawable.button_background_3));

                ImageView closeDialog = dialog.findViewById(R.id.closeDialog);
                final ImageView nextPage = dialog.findViewById(R.id.nextPage);
                final ImageView previousPage = dialog.findViewById(R.id.previousPage);

                final LottieAnimationView lottieAnimationView = dialog.findViewById(R.id.animationView);
                final TextView title = dialog.findViewById(R.id.title);
                final TextView info = dialog.findViewById(R.id.info);

                if(pageIndex == 0){
                    previousPage.setVisibility(View.GONE);
                }

                lottieAnimationView.setAnimation(R.raw.wash_hands);
                title.setText("Wash your hands!");
                info.setText("In light of current events, We request you to wash your hands at regular intervals.");

                closeDialog.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        dialog.dismiss();
                    }
                });

                nextPage.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        pageIndex++;
                        lottieAnimationView.setProgress(0);
                        if(pageIndex == 1){
                            nextPage.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.next));
                            previousPage.setVisibility(View.VISIBLE);
                            lottieAnimationView.setAnimation(R.raw.wear_mask);
                            lottieAnimationView.playAnimation();
                            title.setText("Wear Mask!");
                            info.setText("Wear mask when you're around people.");
                        }else if(pageIndex == 2){
                            lottieAnimationView.setAnimation(R.raw.stay_home);
                            lottieAnimationView.playAnimation();
                            title.setText("Stay at home!");
                            info.setText("Stay at home at all times.");
                            nextPage.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.submit));
                        }else if(pageIndex == 3){
                            dialog.dismiss();
                        }
                    }
                });

                previousPage.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        pageIndex--;
                        lottieAnimationView.setProgress(0);
                        if(pageIndex == 0){
                            nextPage.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.next));
                            previousPage.setVisibility(View.GONE);
                            lottieAnimationView.setAnimation(R.raw.wash_hands);
                            lottieAnimationView.playAnimation();
                            title.setText("Wash your hands!");
                            info.setText("In the light of recent events happening around the world. \nWe request you to wash Your hands at regular intervals.");
                        }else if(pageIndex == 1){
                            lottieAnimationView.setAnimation(R.raw.wear_mask);
                            lottieAnimationView.playAnimation();
                            title.setText("Wear Mask!");
                            info.setText("Wear mask when you're around people.");
                        }
                    }
                });

            }
        });

        logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();

                logout();
            }
        });
    }

    public void logout(){
        new AlertDialog.Builder(context)
                .setTitle("vProtect")
                .setMessage("Are you sure you want to logout ?")
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Toast.makeText(context,"Logging Out!",Toast.LENGTH_LONG).show();
                        // Put it in SharedPreference
                        SharedPreferences pref = getApplicationContext().getSharedPreferences("LoginData", 0); // 0 - for private mode
                        SharedPreferences.Editor editor = pref.edit();
                        editor.clear();
                        editor.commit();

                        // Divert to Login
                        stopService(new Intent(context, BGService.class));

                        Intent intent = new Intent(BeaconActivity.this,LoginActivity.class);
                        startActivity(intent);
                        finish();

                    }
                })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                }).show();
    }

    public void openCirculars(View view) {

        Intent intent = new Intent(BeaconActivity.this, CircularsActivity.class);
        startActivity(intent);
    }

    public void openUserCompliance(View view) {

        Intent intent = new Intent(BeaconActivity.this, UserComplianceSelectionActivity.class);
        startActivity(intent);
    }

    public void openAssistMePage(View view) {

        Intent intent = new Intent(BeaconActivity.this, ChatBotActivity.class);
        startActivity(intent);
    }
}
