package com.vlabs.vprotect;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.gson.Gson;
import com.vlabs.vprotect.Adapters.AssistUserRecyclerAdapter;
import com.vlabs.vprotect.Util.Util;
import com.vlabs.vprotect.model.AppUser;
import com.vlabs.vprotect.model.AssistUserModel;

import java.text.SimpleDateFormat;
import java.util.ArrayList;

public class ChatBotActivity extends AppCompatActivity {

    private Context context;
    private Util util;

    private int orgId, depId, usrId;

    private String response;
    private static RecyclerView.Adapter adapter;
    private RecyclerView.LayoutManager layoutManager;
    private static RecyclerView recyclerView;
    private static ArrayList<AssistUserModel> assistUserModels, assistUserModelArrayList;
    private ProgressDialog progDailog;
    private LinearLayout layNoRecord, layNoInternet;
    private LinearLayout layParent;
    private SimpleDateFormat formatter;
    private AppUser appUser;
    private Button goBack;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat_bot);

        context= this;
        util = new Util();
        assistUserModelArrayList = new ArrayList<>();
        assistUserModels = new ArrayList<>();
        formatter = new SimpleDateFormat("yyyy-MM-dd");

        Gson gson = new Gson();
        SharedPreferences pref = this.getSharedPreferences("LoginData", 0); // 0 - for private mode
        String loggedinuser  = pref.getString("loggedinuser","");
        appUser = gson.fromJson(loggedinuser, AppUser.class);

        orgId = appUser.orgId;
        depId = appUser.depId;
        usrId = appUser.usrId;

        layParent = findViewById(R.id.layParent);
        layNoRecord = findViewById(R.id.layNoRecord);
        layNoInternet = findViewById(R.id.layNoInternet);
        goBack = findViewById(R.id.goBack);

        recyclerView = findViewById(R.id.issued_books_recycler_view);
        recyclerView.setHasFixedSize(true);

        layoutManager = new LinearLayoutManager(context);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());

        AssistUserModel assistUserModel = new AssistUserModel();
        assistUserModel.setQuestion("Please note that information from this chat will be used for monitoring & management of the current health crisis and research in the fight against the virus.");

        AssistUserModel assistUserModel1 = new AssistUserModel();
        assistUserModel1.setqId(1);
        assistUserModel1.setaId("1,2,3,4");
        assistUserModel1.setQuestion("Are you experiencing any of the following symptoms?");
        assistUserModel1.setOption("Cough,Fever,Difficulty in breathing,None of the above");

        AssistUserModel assistUserModel2 = new AssistUserModel();
        assistUserModel2.setqId(2);
        assistUserModel2.setaId("1,2,3,4,5");
        assistUserModel2.setQuestion("Have you ever had any of the following:");
        assistUserModel2.setOption("Diabetes,Hypertension,Lung disease,Heart disease,None of the above");

        AssistUserModel assistUserModel3 = new AssistUserModel();
        assistUserModel3.setqId(3);
        assistUserModel3.setaId("1,2");
        assistUserModel3.setQuestion("Have you travelled anywhere internationally in the last 14 days?");
        assistUserModel3.setOption("Yes,No");

        AssistUserModel assistUserModel4 = new AssistUserModel();
        assistUserModel4.setqId(4);
        assistUserModel4.setaId("1,2,3");
        assistUserModel4.setQuestion("Which of the following apply to you?");
        assistUserModel4.setOption("I have recently interacted or lived with someone who has tested positive for the virus,I am a healthcare worker and I examined a virus confirmed case without protective gear,None of the above");

        AssistUserModel assistUserModel5 = new AssistUserModel();
        assistUserModel5.setqId(5);
        assistUserModel5.setaId("1");
        assistUserModel5.setQuestion("Retake the Self-Assessment test if you develop symptoms or come in contact with a virus confirmed patient.");
        assistUserModel5.setOption("Ok");

        assistUserModelArrayList.add(assistUserModel);
        assistUserModelArrayList.add(assistUserModel1);
        assistUserModelArrayList.add(assistUserModel2);
        assistUserModelArrayList.add(assistUserModel3);
        assistUserModelArrayList.add(assistUserModel4);
        assistUserModelArrayList.add(assistUserModel5);

        assistUserModels.add(assistUserModel);
        assistUserModels.add(assistUserModel1);

        layParent.setVisibility(View.VISIBLE);
        adapter = new AssistUserRecyclerAdapter(context, assistUserModels, assistUserModelArrayList, goBack, appUser);
        recyclerView.setAdapter(adapter);

    }

    public void goBack(View view) {

        finish();
    }
}
