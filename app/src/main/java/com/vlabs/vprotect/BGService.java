package com.vlabs.vprotect;

import android.Manifest;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.Service;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.BatteryManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Looper;
import android.os.Message;
import android.os.PowerManager;
import android.os.Vibrator;
import android.speech.tts.TextToSpeech;
import android.util.Log;
import android.widget.Toast;

import androidx.annotation.RequiresApi;
import androidx.core.app.ActivityCompat;
import androidx.core.app.NotificationCompat;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;
import com.vlabs.vprotect.model.AppUser;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;

import static com.vlabs.vprotect.BeaconActivity.isDebugMode;

public class BGService extends Service implements LocationListener {

    boolean bluetoothSwitchedOFF = false;
    private BluetoothAdapter bluetoothAdapter;
    private Map<String, Integer> deviceMap = new HashMap<>();
    private String bleData = "";
    private List<BluetoothDevice> mDeviceList = new ArrayList<>();
    private LocationManager mLocationManager = null;
    private double latitude, longitude;
    private int batLevel;
    private PowerManager.WakeLock wakeLock;

    private Context context;
    private Timer timer;

    private TextToSpeech t1;

    private Vibrator v;
    private AppUser appUser;
    private int count = 0;

    private int safeCount = 0;
    private String status = "safe";
    static final int NOTIFICATION_ID = 543;

    private int errorCount = 0;

    private boolean isDiscoveryOn = false;
    private final BroadcastReceiver mReceiver = new BroadcastReceiver() {
        public void onReceive(final Context context, Intent intent) {
            String action = intent.getAction();

            if (BluetoothAdapter.ACTION_STATE_CHANGED.equals(action)) {
                final int state = intent.getIntExtra(BluetoothAdapter.EXTRA_STATE, BluetoothAdapter.ERROR);

                if (state == BluetoothAdapter.STATE_ON) {
                    //Toast.makeText(context,"Bluetooth Turned On, Starting ",Toast.LENGTH_LONG).show();

                    bluetoothAdapter.cancelDiscovery();

                    try {
                        Thread.sleep(1000);
                    }catch(Exception e)
                    {

                    }

                    bluetoothAdapter.startDiscovery();
                }

            } else if (BluetoothAdapter.ACTION_DISCOVERY_STARTED.equals(action)) {
                //mDeviceList = new ArrayList<>();
                //showToast("ACTION_DISCOVERY_STARTED");
                //mProgressDlg.show();
                Log.d("vProtect", "Discovery started.");
                errorCount = 0;
            } else if (BluetoothAdapter.ACTION_DISCOVERY_FINISHED.equals(action) && !bluetoothSwitchedOFF) {
                //mProgressDlg.dismiss();
                //showToast("ACTION_DISCOVERY_FINISHED");

                //Intent newIntent = new Intent(MainActivity.this, MainActivity.class);

                //newIntent.putParcelableArrayListExtra("device.list", mDeviceList);

                //startActivity(newIntent);

                // Set the safecount
//                if(errorCount == 0)
//                {
//                    safeCount++;
//                }
//                else
//                {
//                    safeCount = 0;
//                }

                if(errorCount == 0)
                {
                    // Show safe message
                    if(t1.isSpeaking())
                        t1.stop();

                    if(status.equals("unsafe")) {
                        Intent i = new Intent("Distancing");
                        i.putExtra("Status", "Safe");
                        LocalBroadcastManager.getInstance(context).sendBroadcast(i);
                        status = "safe";
                    }
                }


                deviceMap.clear();
                mDeviceList.clear();

                if (isDiscoveryOn)
                    bluetoothAdapter.startDiscovery();

                //bluetoothAdapter.startDiscovery();

                if (isDebugMode) {
                    Toast.makeText(context, "Restarting the search!", Toast.LENGTH_LONG).show();
                }
                Log.d("vProtect", "Discovery finished.");
            } else if (BluetoothDevice.ACTION_FOUND.equals(action)) {// When discovery finds a device
                // Get the BluetoothDevice object from the Intent

                final int rssi = intent.getShortExtra(BluetoothDevice.EXTRA_RSSI, Short.MIN_VALUE);
                final BluetoothDevice device = intent.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE);
                mDeviceList.add(device);

                StringBuilder sb = new StringBuilder();
                String msg = "";

                if (rssi > -54) {
                    errorCount++;
                    deviceMap.put(device.getAddress() + " " + device.getName(), rssi);

                    count = 0;

                    msg = "Social Distancing Needed! Please move away!";
                    sb.append(device.getAddress() + " " + device.getName() + "  " + msg + "\n\n");

                    try {

                        new Handler(Looper.getMainLooper()).post(new Runnable() {
                            @Override
                            public void run() {
                                if (isDebugMode) {
                                    Toast.makeText(context, "(" + mDeviceList.size() + ") Proximity Alert -" + device.getAddress() + " " + device.getName() +" " + device.getType(), Toast.LENGTH_LONG).show();
                                }

                                //Toast.makeText(context, "(" + deviceMap.size() + ") Alert Devices nearby", Toast.LENGTH_SHORT).show();

                                //if (!t1.isSpeaking()) {
                                    //t1.setSpeechRate(0.9f);
                                    //t1.setLanguage(Locale.UK);
                                    String deviceName = "IOS device";
                                    String pronounceName = "IOS device";
                                    try {
                                        deviceName = device.getName();

                                        if (deviceName.equalsIgnoreCase("null")) {
                                            deviceName = "IOS device";
                                        }
                                        else {
                                            String[] splt = deviceName.split("#");
                                            if (splt.length > 1) {
                                                pronounceName = splt[1];
                                            }else
                                            {
                                                pronounceName = deviceName;
                                            }
                                        }
                                    }
                                    catch(Exception e){
                                        deviceName = "IOS device";
                                    }

                                    t1.speak("CORONA Alert ! Please move away from " +  pronounceName + " and maintain Social Distancing! ", TextToSpeech.QUEUE_ADD, null, null);
                                   //t1.speak("Maintain Social Distancing! Please move away from " + device.getName(), TextToSpeech.QUEUE_ADD, null, null);
                                   //t1.speak("Dhyaan De! Kripaaya Surakshit antarr rakhiye! Do gaaj ki doori banaaee rakhiye!", TextToSpeech.QUEUE_ADD, null, null);
                                //}
                                if(status.equals("safe")) {
                                    Intent i = new Intent("Distancing");
                                    i.putExtra("Status", "Danger");
                                    LocalBroadcastManager.getInstance(context).sendBroadcast(i);
                                    status = "unsafe";
                                }

                                // Vibrate for 400 milliseconds
                                v.vibrate(1000);
                                sendData(device,rssi);

                            }
                        });
                    } catch (Exception e) {
                        Toast.makeText(context, "Error: " + e.getMessage(), Toast.LENGTH_LONG).show();
                    }

                } else {
                    new Handler(Looper.getMainLooper()).post(new Runnable() {
                        @Override
                        public void run() {
                            if (isDebugMode) {

                                String deviceName = "IOS device";
                                try {
                                    deviceName = device.getName();
                                    if (deviceName.equalsIgnoreCase("null")) {
                                        deviceName = "IOS device";
                                    }
                                }
                                catch(Exception e){
                                    deviceName = "IOS device";
                                }
                                Toast.makeText(context, "Safe Distance Detected -" + device.getAddress() + " " + deviceName + " " + device.getType(), Toast.LENGTH_LONG).show();
                            }

                            if (count >= 2) {

                            } else {
                                count++;
                            }
                        }
                    });
                }




            }
        }
    };

    public void sendData(final BluetoothDevice device,final int rssi) {
        // Instantiate the RequestQueue.
        RequestQueue queue = Volley.newRequestQueue(this);
        String url = "http://3.6.107.191:8080/saveProximityTransactionV0";

        // Request a string response from the provided URL.
        StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        // Display the first 500 characters of the response string.
                        //Toast.makeText(context,response,Toast.LENGTH_LONG).show();
//                        textView.setText("Response is: "+ response.substring(0,500));
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
//                textView.setText("That didn't work!");
                //Toast.makeText(context,error.getMessage(),Toast.LENGTH_LONG).show();
            }
        }) {
            @Override
            public Map<String, String> getParams() {

                String deviceName = "IOS device";
                String pronounceName = "IOS device";
                String secondUsrId = "0";
                try {
                    deviceName = device.getName();

                    if (deviceName.equalsIgnoreCase("null")) {
                        deviceName = "IOS device";
                    }
                    else {
                        String[] splt = deviceName.split("#");
                        if (splt.length > 1) {
                            secondUsrId = splt[0];
                            pronounceName = splt[1];
                        }else
                        {
                            pronounceName = deviceName;
                        }

                    }
                }
                catch(Exception e){
                    deviceName = "IOS device";
                }

                SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                Map<String, String> params = new HashMap<String, String>();
                params.put("orgId", appUser.orgId.toString());
                params.put("depId", appUser.depId.toString());
                params.put("usrId", appUser.usrId.toString());
                params.put("usrIdSecond", secondUsrId);
                params.put("ptrTimestamp", dateFormat.format(new Date()));
                params.put("remoteUsrBluId", device.getAddress());
                params.put("ptrMessage", appUser.usrFullName + " <-> " + pronounceName + " | " + device.getAddress() + "(" + device.getName() + ") S:" + rssi);
                params.put("latitude", String.valueOf(latitude));
                params.put("longitude", String.valueOf(longitude));

                return params;
            }
        };
        // Add the request to the RequestQueue.
        queue.add(stringRequest);
    }

    public BGService() {


    }

    @Override
    public IBinder onBind(Intent intent) {

        // TODO: Return the communication channel to the service.
        throw new UnsupportedOperationException("Not yet implemented");
    }

    @Override
    public void onCreate() {

        Gson gson = new Gson();
        SharedPreferences pref = this.getSharedPreferences("LoginData", 0); // 0 - for private mode
        String loggedinuser = pref.getString("loggedinuser", "");
        appUser = gson.fromJson(loggedinuser, AppUser.class);

        if (isDebugMode) {
            Toast.makeText(this, "Service was Created", Toast.LENGTH_LONG).show();
        }

        context = this;
        v = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);

        try {
            t1 = new TextToSpeech(getApplicationContext(), new TextToSpeech.OnInitListener() {
                @Override
                public void onInit(int status) {
                    if (status != TextToSpeech.ERROR) {
                        t1.setLanguage(Locale.UK);
                        t1.speak("Hello " + appUser.usrFullName, TextToSpeech.QUEUE_FLUSH, null, null);
                    }
                }
            });


        } catch (Exception e) {
            e.printStackTrace();
        }

        IntentFilter filter = new IntentFilter();
        filter.addAction(BluetoothAdapter.ACTION_STATE_CHANGED);
        filter.addAction(BluetoothAdapter.ACTION_DISCOVERY_STARTED);
        filter.addAction(BluetoothAdapter.ACTION_DISCOVERY_FINISHED);
        filter.addAction(BluetoothDevice.ACTION_FOUND);
        registerReceiver(mReceiver, filter);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O)
            startMyOwnForeground();
        else
            startForeground(1, new Notification());

    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    private void startMyOwnForeground() {
        Log.d("vProtect", "In foreground");
        String NOTIFICATION_CHANNEL_ID = "com.vlabs.vprotect";
        String channelName = "vProtect Background Service";
        NotificationChannel chan = new NotificationChannel(NOTIFICATION_CHANNEL_ID, channelName, NotificationManager.IMPORTANCE_NONE);
        chan.setLightColor(Color.BLUE);
        chan.setLockscreenVisibility(Notification.VISIBILITY_PRIVATE);
        NotificationManager manager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        assert manager != null;
        manager.createNotificationChannel(chan);

        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(getApplicationContext(), NOTIFICATION_CHANNEL_ID);
        Notification notification = notificationBuilder.setOngoing(true)
                .setSmallIcon(R.drawable.notification)
                .setContentTitle("vProtect")
                .setContentText("Stay alert, Stay safe.")
                .setPriority(NotificationManager.IMPORTANCE_MIN)
                .setCategory(Notification.CATEGORY_SERVICE)
                .build();
        startForeground(3, notification);

    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        if (isDebugMode) {
            Toast.makeText(this, "Service Started", Toast.LENGTH_LONG).show();
        }

        count = 0;

//        PowerManager powerManager = (PowerManager) getSystemService(POWER_SERVICE);
//        if (powerManager != null) {
//            wakeLock = powerManager.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK,
//                    "vProtect::vProtectWakeLog");
//            wakeLock.acquire(600000);
//        }

        BluetoothManager bluetoothManager = (BluetoothManager) this.getSystemService(Context.BLUETOOTH_SERVICE);
        bluetoothAdapter = bluetoothManager.getAdapter();

        if (bluetoothAdapter.isDiscovering()) {
            bluetoothAdapter.cancelDiscovery();
        }

        bluetoothAdapter.startDiscovery();
        isDiscoveryOn = true;

        try {

            mLocationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                if (checkSelfPermission(Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && checkSelfPermission(Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                }
            }

            if (mLocationManager != null) {
                boolean gps_enabled = mLocationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
                boolean network_enabled = mLocationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
                if (gps_enabled) {
                    mLocationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 60000, 1000, this);
                    Location location = getLastKnownLocation();

                    if (location != null) {
                        latitude = location.getLatitude();
                        longitude = location.getLongitude();
                    }

                } else if (network_enabled) {
                    mLocationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 60000, 1000, this);

                    Location location = getLastKnownLocation();

                    if (location != null) {
                        latitude = location.getLatitude();
                        longitude = location.getLongitude();
                    }

                } else {

                    latitude = 0;
                    longitude = 0;
                }
            }

        } catch (Exception e) {

            Toast.makeText(context, e.getMessage(), Toast.LENGTH_SHORT).show();
        }

        timer = new Timer();
        timer.scheduleAtFixedRate(new HeartBeatTask(), 0, 3 * 60 * 1000);

        return START_STICKY;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (isDebugMode) {
            Toast.makeText(this, "Service Stopped", Toast.LENGTH_LONG).show();
        }
        Log.d("vProtect", "Service Stopped");
        timer.cancel();
        isDiscoveryOn = false;
        unregisterReceiver(mReceiver);
        this.stopForeground(true);

        try {
            t1 = new TextToSpeech(getApplicationContext(), new TextToSpeech.OnInitListener() {
                @Override
                public void onInit(int status) {
                    if (status != TextToSpeech.ERROR) {
                        t1.setLanguage(Locale.UK);
                        t1.speak("Stopping the Protection", TextToSpeech.QUEUE_FLUSH, null, null);
                    }
                }
            });


        } catch (Exception e) {
            e.printStackTrace();
        }
        stopSelf();
    }

    private final Handler toastHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            Toast.makeText(getApplicationContext(), "TimeTask " + new Date(), Toast.LENGTH_SHORT).show();
        }
    };

    @Override
    public void onLocationChanged(Location location) {

        if (location != null) {
            latitude = location.getLatitude();
            longitude = location.getLongitude();
        }

    }

    @Override
    public void onStatusChanged(String s, int i, Bundle bundle) {

    }

    @Override
    public void onProviderEnabled(String s) {

    }

    @Override
    public void onProviderDisabled(String s) {

    }

    private class HeartBeatTask extends TimerTask {
        public void run() {
            sendHeartBeatData();
            //toastHandler.sendEmptyMessage(0);
        }
    }

    public void sendHeartBeatData() {
        RequestQueue queue = Volley.newRequestQueue(this);
        String url = "http://3.6.107.191:8080/setUsrHeartbeatDateV1";

        StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
            }
        }) {
            @Override
            public Map<String, String> getParams() {
                String usrCurrentStatus = "0";
                try {
                    if(!bluetoothAdapter.isEnabled())
                        usrCurrentStatus = "1";

                } catch (Exception e) {
                }

                BatteryManager bm = (BatteryManager) getSystemService(BATTERY_SERVICE);
                String version = "";

                if (bm != null) {
                    batLevel = bm.getIntProperty(BatteryManager.BATTERY_PROPERTY_CAPACITY);
                } else {
                    batLevel = 0;
                }

                try {
                    PackageInfo pInfo = context.getPackageManager().getPackageInfo(getPackageName(), 0);
                    version = pInfo.versionName;
                } catch (PackageManager.NameNotFoundException e) {
                    e.printStackTrace();
                }

                SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                Map<String, String> params = new HashMap<String, String>();
                params.put("usrId", appUser.usrId.toString());
                params.put("usrHeartbeatDate", dateFormat.format(new Date()));
                params.put("usrCurrentStatus", usrCurrentStatus);
                params.put("latitute", String.valueOf(latitude));
                params.put("longitude", String.valueOf(longitude));
                params.put("batteryLife", String.valueOf(batLevel));
                params.put("appVersion", String.valueOf(version));

                return params;
            }
        };
        // Add the request to the RequestQueue.
        queue.add(stringRequest);
    }

    private Location getLastKnownLocation() {
        mLocationManager = (LocationManager) getApplicationContext().getSystemService(LOCATION_SERVICE);
        List<String> providers = mLocationManager.getProviders(true);
        Location bestLocation = null;
        for (String provider : providers) {
            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                // TODO: Consider calling
                //    ActivityCompat#requestPermissions
                // here to request the missing permissions, and then overriding
                //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                //                                          int[] grantResults)
                // to handle the case where the user grants the permission. See the documentation
                // for ActivityCompat#requestPermissions for more details.
                return null;
            }
            Location l = mLocationManager.getLastKnownLocation(provider);
            if (l == null) {
                continue;
            }
            if (bestLocation == null || l.getAccuracy() < bestLocation.getAccuracy()) {
                // Found best last known location: %s", l);
                bestLocation = l;
            }
        }
        return bestLocation;
    }
}
