package com.vlabs.vprotect;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.app.ProgressDialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TimePicker;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.github.dhaval2404.imagepicker.ImagePicker;
import com.google.gson.Gson;
import com.vlabs.vprotect.Util.Util;
import com.vlabs.vprotect.model.AppUser;

import org.apache.commons.io.FilenameUtils;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import de.hdodenhof.circleimageview.CircleImageView;

public class UserProfileActivity extends AppCompatActivity {

    private Context context;
    private Util util;

    private String response;
    private ProgressDialog progDailog;
    private Button saveDetails;

    private int orgId, depId, usrId;
    private EditText in, out;
    private RadioGroup containmentZone;
    private RadioButton red, green, orange;

    private String selectedContainmentZone = "", enteredIn = "", enteredOut = "";
    private AppUser appUser;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_profile);

        context = this;
        util = new Util();

        Gson gson = new Gson();
        SharedPreferences pref = this.getSharedPreferences("LoginData", 0); // 0 - for private mode
        String loggedinuser  = pref.getString("loggedinuser","");
        appUser = gson.fromJson(loggedinuser, AppUser.class);

        orgId = appUser.orgId;
        depId = appUser.depId;
        usrId = appUser.usrId;

        saveDetails = findViewById(R.id.saveDetails);

        containmentZone = findViewById(R.id.containmentZone);
        in = findViewById(R.id.in);
        out = findViewById(R.id.out);
        red = findViewById(R.id.red);
        green = findViewById(R.id.green);
        orange = findViewById(R.id.orange);

        containmentZone.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int i) {
                if(i == R.id.red){
                    selectedContainmentZone = "R";
                } else if(i == R.id.green){
                    selectedContainmentZone = "G";
                } else if(i == R.id.orange){
                    selectedContainmentZone = "O";
                }
            }
        });

        saveDetails.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                enteredIn = in.getText().toString();
                enteredOut = out.getText().toString();

                if(selectedContainmentZone == null || selectedContainmentZone.equals("")) {
                    Toast.makeText(context, "Please select containment zone.", Toast.LENGTH_SHORT).show();
                }else if(enteredIn == null || enteredIn.equals("")) {
                    Toast.makeText(context, "Please select in time.", Toast.LENGTH_SHORT).show();
                }else if(enteredOut == null || enteredOut.equals("")) {
                    Toast.makeText(context, "Please select in time.", Toast.LENGTH_SHORT).show();
                }

                Thread t1 = new Thread(new Runnable() {
                    @Override
                    public void run() {

                        runOnUiThread(new Runnable() {
                            public void run() {
                                progDailog = new ProgressDialog(context);
                                progDailog.setMessage("Sending data to server...");
                                progDailog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
                                progDailog.setCancelable(false);
                                progDailog.setIndeterminate(false);
                                progDailog.show();
                            }
                        });

                        // Instantiate the RequestQueue.
                        RequestQueue queue = Volley.newRequestQueue(context);
                        String url ="http://3.6.107.191:8080/saveContainmentZoneStatusForUser";

                        // Request a string response from the provided URL.
                        StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                                new Response.Listener<String>() {
                                    @Override
                                    public void onResponse(final String resp) {

                                        runOnUiThread(new Runnable() {
                                            public void run() {

                                                if (progDailog != null) {
                                                    progDailog.dismiss();
                                                }

                                                if (resp.equals("Success")) {

                                                    saveShiftTime();

                                                } else {

                                                    new AlertDialog.Builder(context)
                                                            .setTitle("vProtect")
                                                            .setCancelable(false)
                                                            .setMessage("Something went wrong! Please try again later.")
                                                            .setPositiveButton("Okay", new DialogInterface.OnClickListener() {
                                                                @Override
                                                                public void onClick(DialogInterface dialog, int which) {
                                                                    dialog.dismiss();
                                                                }
                                                            }).show();
                                                }
                                            }
                                        });
                                    }
                                }, new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {
                                response = null;
                            }
                        }){
                            @Override
                            public Map<String, String> getParams()
                            {
                                try {

                                    Map<String, String>  params = new HashMap<String, String>();
                                    params.put("usrId", String.valueOf(usrId));
                                    params.put("containmentZoneStatus", selectedContainmentZone);

                                    return params;
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                                return null;
                            }
                        };
                        // Add the request to the RequestQueue.
                        queue.add(stringRequest);

                    }
                });
                t1.start();

            }
        });

        new GetUserTask().execute();

    }

    public void getInTime(View view) {

        // Get Current Time
        final Calendar c = Calendar.getInstance();
        int mHour = c.get(Calendar.HOUR_OF_DAY);
        int mMinute = c.get(Calendar.MINUTE);

        // Launch Time Picker Dialog
        TimePickerDialog timePickerDialog = new TimePickerDialog(this,
                new TimePickerDialog.OnTimeSetListener() {

                    @Override
                    public void onTimeSet(TimePicker view, int hourOfDay,
                                          int minute) {

                        String time = String.valueOf(hourOfDay).concat(":").concat(String.valueOf(minute)).concat(":").concat("00");

                        in.setText(time);
                    }
                }, mHour, mMinute, false);
        timePickerDialog.show();
    }

    public void getOutTime(View view) {

        // Get Current Time
        final Calendar c = Calendar.getInstance();
        int mHour = c.get(Calendar.HOUR_OF_DAY);
        int mMinute = c.get(Calendar.MINUTE);

        // Launch Time Picker Dialog
        TimePickerDialog timePickerDialog = new TimePickerDialog(this,
                new TimePickerDialog.OnTimeSetListener() {

                    @Override
                    public void onTimeSet(TimePicker view, int hourOfDay,
                                          int minute) {

                        String time = String.valueOf(hourOfDay).concat(":").concat(String.valueOf(minute)).concat(":").concat("00");

                        out.setText(time);

                    }
                }, mHour, mMinute, false);
        timePickerDialog.show();
    }

    public void goBack(View view) {

        finish();
    }

    public void saveShiftTime() {
        Thread t1 = new Thread(new Runnable() {
            @Override
            public void run() {

                runOnUiThread(new Runnable() {
                    public void run() {
                        progDailog = new ProgressDialog(context);
                        progDailog.setMessage("Sending data to server...");
                        progDailog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
                        progDailog.setCancelable(false);
                        progDailog.setIndeterminate(false);
                        progDailog.show();
                    }
                });

                // Instantiate the RequestQueue.
                RequestQueue queue = Volley.newRequestQueue(context);
                String url ="http://3.6.107.191:8080/saveShiftTime";

                // Request a string response from the provided URL.
                StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                        new Response.Listener<String>() {
                            @Override
                            public void onResponse(final String resp) {

                                runOnUiThread(new Runnable() {
                                    public void run() {

                                        if (progDailog != null) {
                                            progDailog.dismiss();
                                        }

                                        if (resp.equals("Success")) {

                                            new AlertDialog.Builder(context)
                                                    .setTitle("vProtect")
                                                    .setCancelable(false)
                                                    .setMessage("Data Successfully saved.")
                                                    .setPositiveButton("Okay", new DialogInterface.OnClickListener() {
                                                        @Override
                                                        public void onClick(DialogInterface dialog, int which) {
//                                                            Intent a = new Intent(AddCustomerComplianceActivity.this, BeaconActivity.class);
//                                                            a.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
//                                                            startActivity(a);

                                                            finish();

                                                        }
                                                    }).show();
                                        } else {

                                            new AlertDialog.Builder(context)
                                                    .setTitle("vProtect")
                                                    .setCancelable(false)
                                                    .setMessage("Something went wrong! Please try again later.")
                                                    .setPositiveButton("Okay", new DialogInterface.OnClickListener() {
                                                        @Override
                                                        public void onClick(DialogInterface dialog, int which) {
                                                            dialog.dismiss();
                                                        }
                                                    }).show();
                                        }
                                    }
                                });
                            }
                        }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        response = null;
                    }
                }){
                    @Override
                    public Map<String, String> getParams()
                    {
                        try {

                            Map<String, String>  params = new HashMap<String, String>();
                            params.put("usrId", String.valueOf(usrId));
                            params.put("inTime", enteredIn);
                            params.put("outTime", enteredOut);

                            return params;
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        return null;
                    }
                };
                // Add the request to the RequestQueue.
                queue.add(stringRequest);

            }
        });
        t1.start();
    }

    public class GetUserTask extends AsyncTask<String, String, String> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progDailog = new ProgressDialog(context);
            progDailog.setMessage("Loading...");
            progDailog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            progDailog.setCancelable(false);
            progDailog.setIndeterminate(false);
            progDailog.show();
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);

            try {
                if (progDailog != null) {
                    progDailog.dismiss();
                }
                if (response != null) {

                    if (!response.isEmpty()) {

                        SharedPreferences pref = getApplicationContext().getSharedPreferences("LoginData", 0); // 0 - for private mode
                        SharedPreferences.Editor editor = pref.edit();

                        editor.putString("loggedinuser", response);
                        editor.commit();

                        Gson gson = new Gson();
                        String loggedinuser  = pref.getString("loggedinuser","");
                        appUser = gson.fromJson(loggedinuser, AppUser.class);

                        selectedContainmentZone = appUser.usrContainmentZoneStatusRog;
                        enteredIn = appUser.usrInTime;
                        enteredOut = appUser.usrOutTime;

                        if (selectedContainmentZone.equals("R")) {
                            red.setChecked(true);
                        } else if (selectedContainmentZone.equals("G")) {
                            green.setChecked(true);
                        } else if (selectedContainmentZone.equals("O")) {
                            orange.setChecked(true);
                        }

                        if (enteredIn != null && !enteredIn.equals("") && !enteredIn.equals("null")) {
                            in.setText(enteredIn);
                        }

                        if (enteredOut != null && !enteredOut.equals("") && !enteredOut.equals("null")) {
                            out.setText(enteredOut);
                        }

                    }

                } else {
                    Toast.makeText(context, "Couldn't reach servers at the moment. Please try again later.", Toast.LENGTH_SHORT).show();
                }

            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        @Override
        protected String doInBackground(String... strings) {
            try {

                response = util.getUser(String.valueOf(usrId));

            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }
    }
}
