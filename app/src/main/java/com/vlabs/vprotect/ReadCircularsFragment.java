package com.vlabs.vprotect;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.Toast;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.gson.Gson;
import com.vlabs.vprotect.Adapters.CircularsRecyclerAdapter;
import com.vlabs.vprotect.Util.Util;
import com.vlabs.vprotect.model.AppUser;
import com.vlabs.vprotect.model.Circular;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

public class ReadCircularsFragment extends Fragment {

    private Context context;
    private Util util;

    private int usrId;

    private String response, selectedDate;
    private static RecyclerView.Adapter adapter;
    private RecyclerView.LayoutManager layoutManager;
    private static RecyclerView recyclerView;
    private static ArrayList<Circular> circularDataArrayList;
    private ProgressDialog progDailog;
    private LinearLayout layNoRecord, layNoInternet;
    private LinearLayout layParent;
    private AppUser appUser;

    public ReadCircularsFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view =  inflater.inflate(R.layout.fragment_read_circulars, container, false);

        context = getActivity();
        util = new Util();

        layParent = view.findViewById(R.id.layParent);
        layNoRecord = view.findViewById(R.id.layNoRecord);

        recyclerView = view.findViewById(R.id.issued_books_recycler_view);
        recyclerView.setHasFixedSize(true);

        layoutManager = new LinearLayoutManager(context);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());

        Gson gson = new Gson();
        SharedPreferences pref = getActivity().getSharedPreferences("LoginData", 0); // 0 - for private mode
        String loggedinuser  = pref.getString("loggedinuser","");
        appUser = gson.fromJson(loggedinuser, AppUser.class);

        usrId = appUser.usrId;

        new GetCustomerCompliances().execute();

        return view;
    }

    public class GetCustomerCompliances extends AsyncTask<String, String, String> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progDailog = new ProgressDialog(context);
            progDailog.setMessage("Sending data to server...");
            progDailog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            progDailog.setCancelable(false);
            progDailog.setIndeterminate(false);
            progDailog.show();
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);

            try {
                if (progDailog != null) {
                    progDailog.dismiss();
                }
                if (response != null) {
                    JSONArray array = new JSONArray(response);

                    circularDataArrayList = new ArrayList<>();

                    if(array.length() > 0) {
                        for (int i=0; i<array.length(); i++) {

                            JSONObject jsonObject = array.getJSONObject(i);

                            Circular circular = new Circular();
                            circular.setMonId(jsonObject.getInt("monId"));
                            circular.setDayId(jsonObject.getInt("dayId"));
                            circular.setCirId(jsonObject.getInt("cirId"));
                            circular.setCirDate(jsonObject.getString("cirDate"));
                            circular.setCirFlagRu(jsonObject.getString("cirFlagRu"));
                            circular.setCirLink(jsonObject.getString("cirLink"));
                            circular.setCirMessage(jsonObject.getString("cirMessage"));
                            circular.setCirReadDate(jsonObject.getString("cirReadDate"));
                            circular.setCirSubject(jsonObject.getString("cirSubject"));

                            circularDataArrayList.add(circular);

                        }

                        if(circularDataArrayList.size() > 0) {

                            layParent.setVisibility(View.VISIBLE);
                            adapter = new CircularsRecyclerAdapter(context, circularDataArrayList, "R", String.valueOf(usrId));
                            recyclerView.setAdapter(adapter);
                        }
                    } else {

                        layParent.setVisibility(View.GONE);
                        layNoRecord.setVisibility(View.VISIBLE);
                    }

                } else {
                    Toast.makeText(context, "Couldn't reach servers at the moment. Please try again later.", Toast.LENGTH_SHORT).show();
                    getActivity().finish();
                }

            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        @Override
        protected String doInBackground(String... strings) {
            try {

                response = util.getCirculars(String.valueOf(usrId), "R");

            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }
    }
}
