package com.vlabs.vprotect.Adapters;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.google.gson.Gson;
import com.vlabs.vprotect.AddUserComplianceActivity;
import com.vlabs.vprotect.R;
import com.vlabs.vprotect.model.Question;
import com.vlabs.vprotect.model.UserComplianceData;

import org.json.JSONArray;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;

public class UserComplianceRecyclerAdapter extends RecyclerView.Adapter<UserComplianceRecyclerAdapter.MyViewHolder> {

    private ArrayList<UserComplianceData> dataSet;
    private ArrayList<Question> questionArrayList;
    private Context context;
    private SimpleDateFormat formatter1, formatter2;

    public static class MyViewHolder extends RecyclerView.ViewHolder {

        TextView checklist, status, assignedOn, deadline, uCompliance;
        ImageView imageView;
        LinearLayout linkLayout;

        public MyViewHolder(View itemView) {
            super(itemView);

            checklist = itemView.findViewById(R.id.checklist);
            status = itemView.findViewById(R.id.status);
            assignedOn = itemView.findViewById(R.id.assignedOn);
            deadline = itemView.findViewById(R.id.deadline);
            uCompliance = itemView.findViewById(R.id.uCompliance);

            imageView = itemView.findViewById(R.id.imageView);

            linkLayout = itemView.findViewById(R.id.linkLayout);
        }
    }

    public UserComplianceRecyclerAdapter(Context context, ArrayList<UserComplianceData> data) {
        this.context = context;
        this.dataSet = data;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent,
                                           int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.row_user_details, parent, false);

        MyViewHolder myViewHolder = new MyViewHolder(view);
        return myViewHolder;
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int listPosition) {

        TextView checklist = holder.checklist;
        TextView status = holder.status;
        TextView assignedOn = holder.assignedOn;
        TextView deadline = holder.deadline;
        TextView uCompliance = holder.uCompliance;

        ImageView imageView = holder.imageView;

        LinearLayout linkLayout = holder.linkLayout;

        formatter1 = new SimpleDateFormat("yyyy-MM-dd");
        formatter2 = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS");

        checklist.setText(dataSet.get(listPosition).getChkName());
        uCompliance.setText(dataSet.get(listPosition).getUscName());

        if(dataSet.get(listPosition).getUscStatus().equals("U")) {
            status.setText("Not Completed");
            imageView.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.user_red));
        } else if(dataSet.get(listPosition).getUscStatus().equals("C")) {
            status.setText("Completed");
            imageView.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.user_green));
        } else if(dataSet.get(listPosition).getUscStatus().equals("H")) {
            status.setText("Partially Completed");
            imageView.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.user_yellow));
        }

        try {

            assignedOn.setText("Assigned on ".concat(formatter1.format(formatter2.parse(dataSet.get(listPosition).getUscAddedDate()))));
            deadline.setText("Should be completed by ".concat(formatter1.format(formatter2.parse(dataSet.get(listPosition).getUscScheduledEndDate()))));

        } catch (Exception e) {

            e.printStackTrace();
            assignedOn.setText("");
            deadline.setText("");
        }

        linkLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                try {

                    if(dataSet.get(listPosition).getChkQuestionJson() != null && !dataSet.get(listPosition).getChkQuestionJson().equals("null") && !dataSet.get(listPosition).getChkQuestionJson().equals("")) {

                        if(!dataSet.get(listPosition).getUscStatus().equals("C")) {
                            JSONArray jsonArray = new JSONArray(dataSet.get(listPosition).getChkQuestionJson());
                            questionArrayList = new ArrayList<>();

                            for (int i=0; i<jsonArray.length(); i++) {

                                JSONObject jsonObject = jsonArray.getJSONObject(i);

                                Question question = new Question();
                                question.setqId(jsonObject.getInt("qId"));
                                question.setqDescription(jsonObject.getString("qDescription"));
                                question.setqType(jsonObject.getString("qType"));
                                question.setqAnswer("");

                                questionArrayList.add(question);
                            }

                            Gson gson = new Gson();

                            Intent intent = new Intent(context, AddUserComplianceActivity.class);
                            intent.putExtra("checkListObject", gson.toJson(dataSet.get(listPosition)));
                            intent.putExtra("questionsArrayListString", gson.toJson(questionArrayList));
                            context.startActivity(intent);
                        }

                    } else {

                        new androidx.appcompat.app.AlertDialog.Builder(context)
                                .setTitle("vProtect")
                                .setCancelable(false)
                                .setMessage("User compliance checklist cannot be empty!")
                                .setPositiveButton("Okay", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        dialog.dismiss();
                                    }
                                }).show();
                    }


                } catch (Exception e) {

                    new androidx.appcompat.app.AlertDialog.Builder(context)
                            .setTitle("vProtect")
                            .setCancelable(false)
                            .setMessage("Error occurred while processing the data.\nPlease show this message to the technical team - ".concat(e.getMessage() == null ? "null" : e.getMessage()))
                            .setPositiveButton("Okay", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.dismiss();
                                }
                            }).show();
                }
            }
        });

    }

    @Override
    public int getItemCount() {
        return dataSet.size();
    }

}

