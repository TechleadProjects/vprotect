package com.vlabs.vprotect.Adapters;

import android.app.ActionBar;
import android.app.Dialog;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.recyclerview.widget.RecyclerView;

import com.developer.filepicker.controller.DialogSelectionListener;
import com.developer.filepicker.model.DialogConfigs;
import com.developer.filepicker.model.DialogProperties;
import com.developer.filepicker.view.FilePickerDialog;
import com.github.dhaval2404.imagepicker.ImagePicker;
import com.vlabs.vprotect.AddUserComplianceActivity;
import com.vlabs.vprotect.R;
import com.vlabs.vprotect.Util.Util;
import com.vlabs.vprotect.model.AppUser;
import com.vlabs.vprotect.model.Question;
import com.vlabs.vprotect.model.UserComplianceData;

import org.apache.commons.io.FilenameUtils;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

public class QuestionListRecyclerAdapter extends RecyclerView.Adapter<QuestionListRecyclerAdapter.MyViewHolder> {

    private ArrayList<Question> dataSet;
    private Context context;
    private String userAnswer;
    private File selectedFile;
    private TextView answerTV;
    private AppUser appUser;
    private UserComplianceData userComplianceData;
    private ArrayList<File> filesTobeUpload;
    private ArrayList<String> fileNames;
    private int position;
    private Util util;

    public static class MyViewHolder extends RecyclerView.ViewHolder {

        TextView question, answer;
        LinearLayout linkLayout;

        public MyViewHolder(View itemView) {
            super(itemView);

            question = itemView.findViewById(R.id.question);
            answer = itemView.findViewById(R.id.answer);

            linkLayout = itemView.findViewById(R.id.linkLayout);
        }
    }

    public QuestionListRecyclerAdapter(Context context, ArrayList<Question> data, AppUser appUser, UserComplianceData userComplianceData) {
        this.context = context;
        this.dataSet = data;
        this.appUser = appUser;
        this.userComplianceData = userComplianceData;

        util = new Util();

        filesTobeUpload = new ArrayList<>();
        fileNames = new ArrayList<>();
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent,
                                           int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.row_question_details, parent, false);

        MyViewHolder myViewHolder = new MyViewHolder(view);
        return myViewHolder;
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int listPosition) {

        TextView question = holder.question;
        final TextView answer = holder.answer;
        LinearLayout linkLayout = holder.linkLayout;

        question.setText(dataSet.get(listPosition).getqDescription());

        linkLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if(dataSet.get(listPosition).getqType().equals("B")) {
                    // yes no

                    final Dialog dialog = new Dialog(context);
                    dialog.setContentView(R.layout.dialog_question_yes_no);
                    dialog.setCancelable(true);
                    dialog.show();
                    Window window = dialog.getWindow();
                    window.setLayout(ActionBar.LayoutParams.WRAP_CONTENT, ActionBar.LayoutParams.WRAP_CONTENT);

                    final ImageView closeDialog = dialog.findViewById(R.id.closeDialog);
                    RadioGroup yesNoStatus = dialog.findViewById(R.id.yesNoStatus);
                    TextView question = dialog.findViewById(R.id.questionName);
                    Button submit = dialog.findViewById(R.id.submit);

                    question.setText(dataSet.get(listPosition).getqDescription());

                    closeDialog.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            dialog.dismiss();
                        }
                    });

                    yesNoStatus.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
                        @Override
                        public void onCheckedChanged(RadioGroup radioGroup, int i) {
                            if(i == R.id.yes) {
                                userAnswer = "Y";
                            } else if(i == R.id.no) {
                                userAnswer = "N";
                            }
                        }
                    });

                    submit.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {

                            if(userAnswer != null && !userAnswer.equals("")) {

                                dialog.dismiss();

                                answer.setVisibility(View.VISIBLE);
                                if (userAnswer.equals("Y")) {
                                    answer.setText("Yes");
                                } else {
                                    answer.setText("No");
                                }

                                dataSet.get(listPosition).setqAnswer(userAnswer);
                            } else {

                                Toast.makeText(context, "Please give answer before submitting!", Toast.LENGTH_SHORT).show();
                            }
                        }
                    });

                } else if(dataSet.get(listPosition).getqType().equals("T")) {
                    // number

                    final Dialog dialog = new Dialog(context);
                    dialog.setContentView(R.layout.dialog_question_temperature);
                    dialog.setCancelable(true);
                    dialog.show();
                    Window window = dialog.getWindow();
                    window.setLayout(ActionBar.LayoutParams.WRAP_CONTENT, ActionBar.LayoutParams.WRAP_CONTENT);

                    final ImageView closeDialog = dialog.findViewById(R.id.closeDialog);
                    final EditText number = dialog.findViewById(R.id.number);
                    TextView question = dialog.findViewById(R.id.questionName);
                    Button submit = dialog.findViewById(R.id.submit);

                    question.setText(dataSet.get(listPosition).getqDescription());

                    closeDialog.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            dialog.dismiss();
                        }
                    });

                    submit.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {

                            userAnswer = number.getText().toString();

                            if(!userAnswer.equals("")) {

                                dialog.dismiss();

                                answer.setVisibility(View.VISIBLE);
                                answer.setText(userAnswer.concat(" °F"));

                                dataSet.get(listPosition).setqAnswer(userAnswer);
                            } else {

                                Toast.makeText(context, "Please give answer before submitting!", Toast.LENGTH_SHORT).show();
                            }
                        }
                    });

                } else if(dataSet.get(listPosition).getqType().equals("N")) {
                    // number

                    final Dialog dialog = new Dialog(context);
                    dialog.setContentView(R.layout.dialog_question_number);
                    dialog.setCancelable(true);
                    dialog.show();
                    Window window = dialog.getWindow();
                    window.setLayout(ActionBar.LayoutParams.WRAP_CONTENT, ActionBar.LayoutParams.WRAP_CONTENT);

                    final ImageView closeDialog = dialog.findViewById(R.id.closeDialog);
                    final EditText number = dialog.findViewById(R.id.number);
                    TextView question = dialog.findViewById(R.id.questionName);
                    Button submit = dialog.findViewById(R.id.submit);

                    question.setText(dataSet.get(listPosition).getqDescription());

                    closeDialog.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            dialog.dismiss();
                        }
                    });

                    submit.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {

                            userAnswer = number.getText().toString();

                            if(!userAnswer.equals("")) {

                                dialog.dismiss();

                                answer.setVisibility(View.VISIBLE);
                                answer.setText(userAnswer);

                                dataSet.get(listPosition).setqAnswer(userAnswer);
                            } else {

                                Toast.makeText(context, "Please give answer before submitting!", Toast.LENGTH_SHORT).show();
                            }
                        }
                    });

                } else if(dataSet.get(listPosition).getqType().equals("S")) {
                    // short ans

                    final Dialog dialog = new Dialog(context);
                    dialog.setContentView(R.layout.dialog_question_short_ans);
                    dialog.setCancelable(true);
                    dialog.show();
                    Window window = dialog.getWindow();
                    window.setLayout(ActionBar.LayoutParams.WRAP_CONTENT, ActionBar.LayoutParams.WRAP_CONTENT);

                    final ImageView closeDialog = dialog.findViewById(R.id.closeDialog);
                    final EditText text = dialog.findViewById(R.id.text);
                    TextView question = dialog.findViewById(R.id.questionName);
                    Button submit = dialog.findViewById(R.id.submit);

                    question.setText(dataSet.get(listPosition).getqDescription());

                    closeDialog.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            dialog.dismiss();
                        }
                    });

                    submit.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {

                            userAnswer = text.getText().toString();

                            if(!userAnswer.equals("")) {

                                dialog.dismiss();

                                answer.setVisibility(View.VISIBLE);
                                answer.setText(userAnswer);

                                dataSet.get(listPosition).setqAnswer(userAnswer);
                            } else {

                                Toast.makeText(context, "Please give answer before submitting!", Toast.LENGTH_SHORT).show();
                            }
                        }
                    });

                } else if(dataSet.get(listPosition).getqType().equals("P")) {
                    // photo upload

                    answerTV = answer;
                    position = listPosition;
                    ImagePicker.Companion.with((AddUserComplianceActivity) context).crop().compress(1024).maxResultSize(1080, 1080).start();

                } else if(dataSet.get(listPosition).getqType().equals("D")) {
                    // document upload

                    DialogProperties properties = new DialogProperties();
                    properties.selection_mode = DialogConfigs.SINGLE_MODE;
                    properties.selection_type = DialogConfigs.FILE_SELECT;
                    properties.root = new File(DialogConfigs.DEFAULT_DIR);
                    properties.error_dir = new File(DialogConfigs.DEFAULT_DIR);
                    properties.offset = new File(DialogConfigs.DEFAULT_DIR);
                    properties.extensions = null;
                    properties.show_hidden_files = false;

                    FilePickerDialog dialog = new FilePickerDialog(context,properties);
                    dialog.setTitle("Select File(s)");

                    dialog.setDialogSelectionListener(new DialogSelectionListener() {
                        @Override
                        public void onSelectedFilePaths(String[] files) {

                            if (files.length > 0) {

                                selectedFile = new File(files[0]);
                                filesTobeUpload.add(selectedFile);

                                String[] pathArray = selectedFile.getPath().split("/");
                                String displayName = pathArray[pathArray.length - 1];
                                answer.setVisibility(View.VISIBLE);
                                answer.setText(displayName);

                                SimpleDateFormat ft = new SimpleDateFormat ("yyyy.MM.dd-hh:mm:ss:SSS");
                                String ext = FilenameUtils.getExtension(displayName);

                                String fileName = String.valueOf(appUser.orgId).concat(String.valueOf(appUser.depId)).concat(String.valueOf(appUser.usrId)).concat(String.valueOf(userComplianceData.getUscId())).concat(String.valueOf(userComplianceData.getChkId())).concat(String.valueOf(dataSet.get(position).getqId())).concat("_").concat(ft.format(new Date())).concat(".").concat(ext);
                                fileNames.add(fileName);

                                userAnswer = "http://s3.ap-southeast-1.amazonaws.com/".concat(util.getBucketName()).concat("/").concat(fileName);

                                dataSet.get(listPosition).setqAnswer(userAnswer);
                            } else {
                                Toast.makeText(context, "Could not select image.", Toast.LENGTH_SHORT).show();
                            }

                        }
                    });

                    dialog.show();

                }
            }
        });

    }

    @Override
    public int getItemCount() {
        return dataSet.size();
    }

    public ArrayList<Question> getAnsSet() {
        return dataSet;
    }

    public ArrayList<File> getFilesToBeUpload() {
        return filesTobeUpload;
    }
    public ArrayList<String> getFileNames() {
        return fileNames;
    }

    public void setSelectedFile(File file) {
        selectedFile = file;
        filesTobeUpload.add(selectedFile);

        String[] pathArray = selectedFile.getPath().split("/");
        String displayName = pathArray[pathArray.length - 1];
        answerTV.setVisibility(View.VISIBLE);
        answerTV.setText(displayName);

        SimpleDateFormat ft = new SimpleDateFormat ("yyyy.MM.dd-hh:mm:ss:SSS");
        String ext = FilenameUtils.getExtension(displayName);

        String fileName = String.valueOf(appUser.orgId).concat(String.valueOf(appUser.depId)).concat(String.valueOf(appUser.usrId)).concat(String.valueOf(userComplianceData.getUscId())).concat(String.valueOf(userComplianceData.getChkId())).concat(String.valueOf(dataSet.get(position).getqId())).concat("_").concat(ft.format(new Date())).concat(".").concat(ext);
        fileNames.add(fileName);

        userAnswer = "http://s3.ap-southeast-1.amazonaws.com/".concat(util.getBucketName()).concat("/").concat(fileName);

        dataSet.get(position).setqAnswer(userAnswer);
    }

}

