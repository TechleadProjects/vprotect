package com.vlabs.vprotect.Adapters;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.appcompat.app.AlertDialog;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.vlabs.vprotect.CircularsActivity;
import com.vlabs.vprotect.R;
import com.vlabs.vprotect.model.Circular;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

public class CircularsRecyclerAdapter extends RecyclerView.Adapter<CircularsRecyclerAdapter.MyViewHolder> {

    private ArrayList<Circular> dataSet;
    private Context context;
    private String status;
    private ProgressDialog progDailog;
    private String usrId;
    private SimpleDateFormat formatter1, formatter2;

    public static class MyViewHolder extends RecyclerView.ViewHolder {

        TextView message, subject, attach, markRead, date;

        public MyViewHolder(View itemView) {
            super(itemView);

            message = itemView.findViewById(R.id.message);
            subject = itemView.findViewById(R.id.subject);
            attach = itemView.findViewById(R.id.attach);
            markRead = itemView.findViewById(R.id.markRead);
            date = itemView.findViewById(R.id.date);
        }
    }

    public CircularsRecyclerAdapter(Context context, ArrayList<Circular> data, String status, String usrId) {
        this.context = context;
        this.dataSet = data;
        this.status = status;
        this.usrId = usrId;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent,
                                           int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.row_circular_details, parent, false);

        MyViewHolder myViewHolder = new MyViewHolder(view);
        return myViewHolder;
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int listPosition) {

        TextView message = holder.message;
        TextView subject = holder.subject;
        TextView markRead = holder.markRead;
        TextView attach = holder.attach;
        TextView date = holder.date;

        formatter1 = new SimpleDateFormat("yyyy-MM-dd");
        formatter2 = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS");

        if(status.equals("R")) {
            markRead.setVisibility(View.GONE);
        }

        message.setText(dataSet.get(listPosition).getCirMessage());
        subject.setText(dataSet.get(listPosition).getCirSubject());

        try {

            date.setText(formatter1.format(formatter2.parse(dataSet.get(listPosition).getCirDate())));

        } catch (Exception e) {
            e.printStackTrace();
        }

        markRead.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            updateStatus(listPosition);

            }
        });

        attach.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if(dataSet.get(listPosition).getCirLink() == null || dataSet.get(listPosition).getCirLink().equals("null") || dataSet.get(listPosition).getCirLink().equals("")) {

                    new androidx.appcompat.app.AlertDialog.Builder(context)
                            .setTitle("vProtect")
                            .setCancelable(false)
                            .setMessage("Attachment not present for this circular!")
                            .setPositiveButton("Okay", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.dismiss();
                                }
                            }).show();
                } else {

                    Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(dataSet.get(listPosition).getCirLink()));
                    context.startActivity(browserIntent);
                }
            }
        });

    }

    @Override
    public int getItemCount() {
        return dataSet.size();
    }

    public void updateStatus(final int pos) {

        Thread t1 = new Thread(new Runnable() {
            @Override
            public void run() {

                ((CircularsActivity) context).runOnUiThread(new Runnable() {
                    public void run() {
                        progDailog = new ProgressDialog(context);
                        progDailog.setMessage("Sending data to server...");
                        progDailog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
                        progDailog.setCancelable(false);
                        progDailog.setIndeterminate(false);
                        progDailog.show();
                    }
                });

                // Instantiate the RequestQueue.
                RequestQueue queue = Volley.newRequestQueue(context);
                String url ="http://3.6.107.191:8080/updateCircularStatus";

                // Request a string response from the provided URL.
                StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                        new Response.Listener<String>() {
                            @Override
                            public void onResponse(final String resp) {

                                ((CircularsActivity) context).runOnUiThread(new Runnable() {
                                    public void run() {

                                        if (progDailog != null) {
                                            progDailog.dismiss();
                                        }

                                        if (resp.equals("Success")) {

                                            new AlertDialog.Builder(context)
                                                    .setTitle("vProtect")
                                                    .setCancelable(false)
                                                    .setMessage("Data Successfully saved.")
                                                    .setPositiveButton("Okay", new DialogInterface.OnClickListener() {
                                                        @Override
                                                        public void onClick(DialogInterface dialog, int which) {
                                                            ((CircularsActivity) context).finish();
                                                            context.startActivity(((CircularsActivity) context).getIntent());
                                                        }
                                                    }).show();
                                        } else {

                                            new AlertDialog.Builder(context)
                                                    .setTitle("vProtect")
                                                    .setCancelable(false)
                                                    .setMessage("Something went wrong! Please try again later.")
                                                    .setPositiveButton("Okay", new DialogInterface.OnClickListener() {
                                                        @Override
                                                        public void onClick(DialogInterface dialog, int which) {
                                                            dialog.dismiss();
                                                        }
                                                    }).show();
                                        }
                                    }
                                });
                            }
                        }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                        if (progDailog != null) {
                            progDailog.dismiss();
                        }

                        new AlertDialog.Builder(context)
                                .setTitle("vProtect")
                                .setCancelable(false)
                                .setMessage("Something went wrong! Please try again later.")
                                .setPositiveButton("Okay", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        dialog.dismiss();
                                    }
                                }).show();
                    }
                }){
                    @Override
                    public Map<String, String> getParams()
                    {
                        try {

                            formatter1 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

                            Map<String, String> params = new HashMap<String, String>();
                            params.put("monId", String.valueOf(dataSet.get(pos).getMonId()));
                            params.put("dayId", String.valueOf(dataSet.get(pos).getDayId()));
                            params.put("usrId", String.valueOf(usrId));
                            params.put("cirId", String.valueOf(dataSet.get(pos).getCirId()));
                            params.put("cirStatus", "R");
                            params.put("cirReadDate", formatter1.format(new Date()));

                            return params;
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        return null;
                    }
                };
                // Add the request to the RequestQueue.
                queue.add(stringRequest);

            }
        });
        t1.start();

    }

}

