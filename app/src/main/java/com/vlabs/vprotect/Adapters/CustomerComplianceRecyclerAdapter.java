package com.vlabs.vprotect.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.squareup.picasso.Picasso;
import com.vlabs.vprotect.R;
import com.vlabs.vprotect.model.ComplianceData;

import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;

public class CustomerComplianceRecyclerAdapter extends RecyclerView.Adapter<CustomerComplianceRecyclerAdapter.MyViewHolder> {

    private ArrayList<ComplianceData> dataSet;
    private Context context;

    public static class MyViewHolder extends RecyclerView.ViewHolder {

        TextView name, phone, email, in, out;
        CircleImageView profilePhoto;
        LinearLayout linkLayout;

        public MyViewHolder(View itemView) {
            super(itemView);

            name = itemView.findViewById(R.id.name);
            phone = itemView.findViewById(R.id.phone);
            email = itemView.findViewById(R.id.email);
            in = itemView.findViewById(R.id.inTime);
            out = itemView.findViewById(R.id.outTime);

            profilePhoto = itemView.findViewById(R.id.imageView);

            linkLayout = itemView.findViewById(R.id.linkLayout);
        }
    }

    public CustomerComplianceRecyclerAdapter(Context context, ArrayList<ComplianceData> data) {
        this.context = context;
        this.dataSet = data;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent,
                                           int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.row_customer_details, parent, false);

        MyViewHolder myViewHolder = new MyViewHolder(view);
        return myViewHolder;
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int listPosition) {

        TextView name = holder.name;
        TextView phone = holder.phone;
        TextView email = holder.email;
        TextView in = holder.in;
        TextView out = holder.out;

        CircleImageView profilePhoto = holder.profilePhoto;

        LinearLayout linkLayout = holder.linkLayout;

        name.setText(dataSet.get(listPosition).getName());
        phone.setText(dataSet.get(listPosition).getPhone());
        email.setText(dataSet.get(listPosition).getEmail());
        in.setText(dataSet.get(listPosition).getIn());
        out.setText(dataSet.get(listPosition).getOut());

        Picasso.with(context).load(dataSet.get(listPosition).getPhotoUrl()).into(profilePhoto);

        linkLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


            }
        });

    }

    @Override
    public int getItemCount() {
        return dataSet.size();
    }

}

