package com.vlabs.vprotect.Adapters;

import android.app.ProgressDialog;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.vlabs.vprotect.ChatBotActivity;
import com.vlabs.vprotect.R;
import com.vlabs.vprotect.model.AppUser;
import com.vlabs.vprotect.model.AssistUserModel;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

public class AssistUserRecyclerAdapter extends RecyclerView.Adapter<AssistUserRecyclerAdapter.MyViewHolder> {

    private ArrayList<AssistUserModel> dataSet, wholeData;
    private String finalQuestionIdString = "", finalAnsString = "", finalAnsIdString = "";
    private Context context;
    private String status;
    private int count=0;
    private Button goBack;
    private ProgressDialog progDailog;
    private SimpleDateFormat formatter1;
    private AppUser appUser;

    public static class MyViewHolder extends RecyclerView.ViewHolder {

        TextView question, answer;
        RadioGroup chipGroup;
        LinearLayout linkLayout;

        public MyViewHolder(View itemView) {
            super(itemView);

            question = itemView.findViewById(R.id.question);
            answer = itemView.findViewById(R.id.answer);

            chipGroup = itemView.findViewById(R.id.chipGroup);

            linkLayout = itemView.findViewById(R.id.linkLayout);
        }
    }

    public AssistUserRecyclerAdapter(Context context, ArrayList<AssistUserModel> data, ArrayList<AssistUserModel> wholeData, Button goBack, AppUser appUser) {
        this.context = context;
        this.dataSet = data;
        this.wholeData = wholeData;
        this.goBack = goBack;
        this.appUser = appUser;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent,
                                           int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.row_assist_user, parent, false);

        MyViewHolder myViewHolder = new MyViewHolder(view);
        return myViewHolder;
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int listPosition) {

        TextView question = holder.question;
        final TextView answer = holder.answer;
        RadioGroup chipGroup = holder.chipGroup;
        LinearLayout linkLayout = holder.linkLayout;

        question.setText(dataSet.get(listPosition).getQuestion());

        if (dataSet.get(listPosition).getOption() == null || dataSet.get(listPosition).getOption().equals("")) {
            chipGroup.setVisibility(View.GONE);
            return;
        }

        String[] options = dataSet.get(listPosition).getOption().split(",");

        for (int i=0; i<options.length; i++) {

            RadioButton chip = new RadioButton(context);
            chip.setText(options[i]);
            chip.setClickable(true);
            chip.animate();
            int id = View.generateViewId();
            chip.setId(id);

            String chipIds = dataSet.get(listPosition).getOptionIds();
            if (chipIds != null && !chipIds.equals("")) {

                chipIds = chipIds.concat(",").concat(String.valueOf(id));
                dataSet.get(listPosition).setOptionIds(chipIds);

            } else {

                dataSet.get(listPosition).setOptionIds(String.valueOf(id));

            }

            chipGroup.addView(chip);
        }

        chipGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int i) {

                radioGroup.setVisibility(View.GONE);

                ArrayList<String> ids = new ArrayList<>(Arrays.asList(dataSet.get(listPosition).getOptionIds().split(",")));
                ArrayList<String> options = new ArrayList<>(Arrays.asList(dataSet.get(listPosition).getOption().split(",")));
                ArrayList<String> answerIds = new ArrayList<>(Arrays.asList(dataSet.get(listPosition).getaId().split(",")));
                int index = ids.indexOf(String.valueOf(i));
                String ans = options.get(index);
                String ansId = answerIds.get(index);
                String quesId = String.valueOf(dataSet.get(listPosition).getqId());

                answer.setVisibility(View.VISIBLE);
                answer.setText(ans);

                if (finalQuestionIdString.equals("")) {
                    finalQuestionIdString = finalQuestionIdString.concat(quesId);
                } else {
                    finalQuestionIdString = finalQuestionIdString.concat(",").concat(quesId);
                }

                if (finalAnsIdString.equals("")) {
                    finalAnsIdString = finalAnsIdString.concat(ansId);
                } else {
                    finalAnsIdString = finalAnsIdString.concat(",").concat(ansId);
                }

                if (finalAnsString.equals("")) {
                    finalAnsString = finalAnsString.concat(ans);
                } else {
                    finalAnsString = finalAnsString.concat("#").concat(ans);
                }

                if ((quesId.equals("1") && ansId.equals("4")) || (quesId.equals("2") && ansId.equals("5")) || (quesId.equals("3") && ansId.equals("2")) || (quesId.equals("4") && ansId.equals("3"))) {

                    count++;
                }

                if (listPosition + 1 < wholeData.size()) {
                    dataSet.clear();

                    if (listPosition + 1 == wholeData.size() - 1) {

                        if (count < 2) {

                            status = "H";
                            wholeData.get(wholeData.size() - 1).setQuestion("You're advised for testing as your infection risk is high. Please call the toll-free help line 1075 to schedule your testing.");

                        } else if (count == 2 || count == 3){

                            status = "M";
                            wholeData.get(wholeData.size() - 1).setQuestion("Your infection risk is low. Do remember that currently it is safer to consult a doctor through phone or video than to visit a hospital.\n\nWe recommend that you stay at home to avoid any chance of exposure to the Novel Corona virus.");

                        } else if (count == 4) {

                            status = "L";
                            wholeData.get(wholeData.size() - 1).setQuestion("Your infection risk is low.\n\nRetake the Self-Assessment test if you develop symptoms or come in contact with a confirmed patient.");

                        }

                    }

                    for (int p=0; p<=listPosition; p++) {
                        dataSet.add(wholeData.get(p));
                    }

                    dataSet.add(wholeData.get(listPosition + 1));

                    notifyDataSetChanged();
                } else {

                    System.out.println("Status : " + status);
                    System.out.println("QuesIds : " + finalQuestionIdString);
                    System.out.println("Ans : " + finalAnsString);
                    System.out.println("AnsId : " + finalAnsIdString);

                    updateHealthStatus(count);

                }
            }
        });

    }

    @Override
    public int getItemCount() {
        return dataSet.size();
    }

    public void updateHealthStatus(final int count) {

        Thread t1 = new Thread(new Runnable() {
            @Override
            public void run() {

                ((ChatBotActivity) context).runOnUiThread(new Runnable() {
                    public void run() {
                        progDailog = new ProgressDialog(context);
                        progDailog.setMessage("Sending data to server...");
                        progDailog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
                        progDailog.setCancelable(false);
                        progDailog.setIndeterminate(false);
                        progDailog.show();
                    }
                });

                // Instantiate the RequestQueue.
                RequestQueue queue = Volley.newRequestQueue(context);
                String url ="http://3.6.107.191:8080/updateUserHealthStatus";

                // Request a string response from the provided URL.
                StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                        new Response.Listener<String>() {
                            @Override
                            public void onResponse(final String resp) {

                                ((ChatBotActivity) context).runOnUiThread(new Runnable() {
                                    public void run() {

                                        if (progDailog != null) {
                                            progDailog.dismiss();
                                        }

                                        goBack.setVisibility(View.VISIBLE);

                                        goBack.setOnClickListener(new View.OnClickListener() {
                                            @Override
                                            public void onClick(View view) {
                                                ((ChatBotActivity)context).finish();
                                            }
                                        });

                                        if (resp.equals("Success")) {

//                                            new AlertDialog.Builder(context)
//                                                    .setTitle("vProtect")
//                                                    .setCancelable(false)
//                                                    .setMessage("Data Successfully saved.")
//                                                    .setPositiveButton("Okay", new DialogInterface.OnClickListener() {
//                                                        @Override
//                                                        public void onClick(DialogInterface dialog, int which) {
//                                                            ((CircularsActivity) context).finish();
//                                                            context.startActivity(((CircularsActivity) context).getIntent());
//                                                        }
//                                                    }).show();
                                        } else {

//                                            new AlertDialog.Builder(context)
//                                                    .setTitle("vProtect")
//                                                    .setCancelable(false)
//                                                    .setMessage("Something went wrong! Please try again later.")
//                                                    .setPositiveButton("Okay", new DialogInterface.OnClickListener() {
//                                                        @Override
//                                                        public void onClick(DialogInterface dialog, int which) {
//                                                            dialog.dismiss();
//                                                        }
//                                                    }).show();
                                        }
                                    }
                                });
                            }
                        }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                        if (progDailog != null) {
                            progDailog.dismiss();
                        }

//                        new AlertDialog.Builder(context)
//                                .setTitle("vProtect")
//                                .setCancelable(false)
//                                .setMessage("Something went wrong! Please try again later.")
//                                .setPositiveButton("Okay", new DialogInterface.OnClickListener() {
//                                    @Override
//                                    public void onClick(DialogInterface dialog, int which) {
//                                        dialog.dismiss();
//                                    }
//                                }).show();
                    }
                }){
                    @Override
                    public Map<String, String> getParams()
                    {
                        try {

                            formatter1 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

                            Map<String, String> params = new HashMap<String, String>();
                            params.put("orgId", String.valueOf(appUser.orgId));
                            params.put("depId", String.valueOf(appUser.depId));
                            params.put("usrId", String.valueOf(appUser.usrId));
                            params.put("score", String.valueOf(count));
                            params.put("riskStatusHml", status);
                            params.put("questionAnswerJson", finalAnsString);
                            params.put("addedDate", formatter1.format(new Date()));

                            return params;
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        return null;
                    }
                };
                // Add the request to the RequestQueue.
                queue.add(stringRequest);

            }
        });
        t1.start();

    }

}

